      subroutine read_thermal_resistances(data_file,Ntr,R)
      include 'max.inc'
c     
c     Purpose: to read input thermal resistances
c     
c     Input:
c       + data_file: file to read
c     
c     Output:
c       + Ntr: numher of thermal resistances
c       + R: thermal resistances [m².K.W⁻¹]
c     
c     I/O
      character*(Nchar_mx) data_file
      integer Ntr
      double precision R(1:Nsolid_mx)
c     temp
      integer file_ios,line_ios,i
      logical keep_going
      double precision tmp
c     label
      character*(Nchar_mx) label
      label='subroutine read_thermal_resistances'

      open(11,file=trim(data_file),status='old',iostat=file_ios)
      if (file_ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(data_file)
         stop
      endif
      do i=1,3
         read(11,*)
      enddo                     ! i
      Ntr=0
      keep_going=.true.
      do while (keep_going)
         read(11,*,iostat=line_ios) tmp1
         if (line_ios.ne.0) then
            keep_going=.false.
         else
            Ntr=Ntr+1
            if (Ntr.gt.Ntr_mx) then
               call error(label)
               write(*,*) 'Ntr_mx has been reached'
               stop
            endif
            R(Ntr)=tmp1
            if (R(Ntr).lt.0.0D+0) then
               call error(label)
               write(*,*) 'R(',Ntr,')=',R(Ntr)
               write(*,*) 'should be >= 0'
               stop
            endif
         endif                  ! line_ios
      enddo                     ! while (keep_going)
      close(11)

      return
      end
