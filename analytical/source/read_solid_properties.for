      subroutine read_solid_properties(data_file,Nsolid,e_solid,lambda_solid,rho_solid,C_solid,P_solid,Tinit_solid)
      include 'max.inc'
c     
c     Purpose: to read input solid thermophysical properties
c     
c     Input:
c       + data_file: file to read
c     
c     Output:
c       + Nsolid: number of solids
c       + e_solid: thickness [m]
c       + lambda_solid: conductivity [W.m⁻¹.K⁻¹]
c       + rho_solid: volumic mass [kg.m⁻³]
c       + C_solid: massic specific heat [J.kg⁻1K⁻¹]
c       + P_solid: term source [W.⁻³]
c       + Tinit_solid: initial temperature [K]
c     
c     I/O
      character*(Nchar_mx) data_file
      integer Nsolid
      double precision e_solid(1:Nsolid_mx)
      double precision lambda_solid(1:Nsolid_mx)
      double precision rho_solid(1:Nsolid_mx)
      double precision C_solid(1:Nsolid_mx)
      double precision P_solid(1:Nsolid_mx)
      double precision Tinit_solid(1:Nsolid_mx)
c     temp
      integer file_ios,line_ios,i
      logical keep_going
      double precision tmp1,tmp2,tmp3,tmp4,tmp5,tmp6
c     label
      character*(Nchar_mx) label
      label='subroutine read_solid_properties'

      open(11,file=trim(data_file),status='old',iostat=file_ios)
      if (file_ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(data_file)
         stop
      endif
      do i=1,3
         read(11,*)
      enddo                     ! i
      Nsolid=0
      keep_going=.true.
      do while (keep_going)
         read(11,*,iostat=line_ios) tmp1,tmp2,tmp3,tmp4,tmp5,tmp6
         if (line_ios.ne.0) then
            keep_going=.false.
         else
            Nsolid=Nsolid+1
            if (Nsolid.gt.Nsolid_mx) then
               call error(label)
               write(*,*) 'Nsolid_mx has been reached'
               stop
            endif
            e_solid(Nsolid)=tmp1
            if (e_solid(Nsolid).le.0.0D+0) then
               call error(label)
               write(*,*) 'e_solid(',Nsolid,')=',e_solid(Nsolid)
               write(*,*) 'should be > 0'
               stop
            endif
            lambda_solid(Nsolid)=tmp2
            if (lambda_solid(Nsolid).le.0.0D+0) then
               call error(label)
               write(*,*) 'lambda_solid(',Nsolid,')=',lambda_solid(Nsolid)
               write(*,*) 'should be > 0'
               stop
            endif
            rho_solid(Nsolid)=tmp3
            if (rho_solid(Nsolid).le.0.0D+0) then
               call error(label)
               write(*,*) 'rho_solid(',Nsolid,')=',rho_solid(Nsolid)
               write(*,*) 'should be > 0'
               stop
            endif
            C_solid(Nsolid)=tmp4
            if (C_solid(Nsolid).le.0.0D+0) then
               call error(label)
               write(*,*) 'C_solid(',Nsolid,')=',C_solid(Nsolid)
               write(*,*) 'should be > 0'
               stop
            endif
            P_solid(Nsolid)=tmp5
            Tinit_solid(Nsolid)=tmp6
            if (Tinit_solid(Nsolid).le.0.0D+0) then
               call error(label)
               write(*,*) 'Tinit_solid(',Nsolid,')=',Tinit_solid(Nsolid)
               write(*,*) 'should be > 0'
               stop
            endif
         endif                  ! line_ios
      enddo                     ! while (keep_going)
      close(11)
      if (Nsolid.le.0) then
         call error(label)
         write(*,*) 'Nsolid=',Nsolid
         write(*,*) 'should be > 0'
         stop
      endif

      return
      end
