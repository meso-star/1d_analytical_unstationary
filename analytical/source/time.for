      subroutine time(itime,Ntime,alpha,total_time,t)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the value of time for a given time step
c     
c     Input:
c       + itime: index of the time step
c       + Ntime: total number of time steps
c       + alpha: delta(t)_i=alpha*delta(t)_i-1
c       + total_time: total time [s]
c
c     Outut:
c       + t: value of time [s]
c     
c     I/O
      integer itime
      integer Ntime
      double precision alpha
      double precision total_time
      double precision t
c     temp
      integer it
      double precision delta_t1
c     label
      character*(Nchar_mx) label
      label='subroutine time'

      if (alpha.eq.1) then
         delta_t1=total_time/dble(Ntime)
      else
         delta_t1=total_time*(alpha-1.0D+0)/(alpha**dble(Ntime)-1.0D+0)
      endif
      t=0.0D+0
      if (itime.gt.0) then
         do it=1,itime
            t=t+delta_t1*alpha**(it-1)
         enddo                  !it
      endif

      return
      end
