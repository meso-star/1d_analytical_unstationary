      subroutine read_data(data_file,
     &     R1,hc1,hc2,hr2,Df,Tf0,Tf2,Tf3,Trad,Trad_ref,Nmesh,Ntime,alpha,total_time,Nsample,ddf)
      implicit none
      include 'max.inc'
      include 'param.inc'
c
c     Purpose: to read the "data.in" input file
c
c     Input:
c       + data_file: name of the data file to read
c
c     Output:
c       + R1: radius of the internal cavity [m]
c       + hc1: convective exchange coefficient of internal boundary [W/(m^2.K)]
c       + hc2: convective exchange coefficient of external boundary [W/(m^2.K)]
c       + hr2: radiative exchange coefficient of external boundary [W/(m^2.K)]
c       + Df: volumic rate of fluid [m^3/s]
c       + Tf0: initial temperature of the fluid [K]
c       + Tf2: temperature of the external fluid [K]
c       + Tf3: temperature of the replacement fluid [K]
c       + Trad: external radiative exchange temperature [K]
c       + Trad_ref: reference radiative temperature [K]
c       + Nmesh: number of cells for the shell
c       + Ntime: number of time points
c       + alpha: delta(t)_i=alpha*delta(t)_i-1
c       + total_time: final time [s]
c       + Nsample: number of samples for Stardis
c       + ddf: value of delta=e/ddf in Stardis
c
c     I/O
      character*(Nchar_mx) data_file
      double precision R1
      double precision hc1
      double precision hc2
      double precision hr2
      double precision Df
      double precision Tf0
      double precision Tf2
      double precision Tf3
      double precision Trad
      double precision Trad_ref
      integer Nmesh
      integer Ntime
      double precision alpha
      double precision total_time
      integer Nsample
      double precision ddf
c     temp
      integer i,ios
c     label
      character*(Nchar_mx) label
      label='subroutine read_data'

      open(10,file=trim(data_file),
     &     status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File could not be found:'
         write(*,*) trim(data_file)
         stop
      else
         do i=1,6
            read(10,*)
         enddo ! i
         read(10,*) R1
         read(10,*)
         read(10,*) hc1
         read(10,*)
         read(10,*) hc2
         read(10,*)
         read(10,*) Trad_ref
         read(10,*)
         read(10,*) Df
         do i=1,5
            read(10,*)
         enddo ! i
         read(10,*) Tf0
         do i=1,5
            read(10,*)
         enddo ! i
         read(10,*) Tf2
         read(10,*)
         read(10,*) Tf3
         read(10,*)
         read(10,*) Trad
         do i=1,5
            read(10,*)
         enddo ! i
         read(10,*) Nmesh
         read(10,*)
         read(10,*) Ntime
         read(10,*)
         read(10,*) alpha
         read(10,*)
         read(10,*) total_time
         do i=1,5
            read(10,*)
         enddo ! i
         read(10,*) Nsample
         read(10,*)
         read(10,*) ddf
      endif
      close(10)

      if (R1.le.0.0D+0) then
         call error(label)
         write(*,*) 'R1=',R1,' m'
         write(*,*) 'should be positive'
         stop
      endif
      if (hc1.le.0.0D+0) then
         call error(label)
         write(*,*) 'hc1=',hc1,' W.m⁻².K⁻¹'
         write(*,*) 'should be positive'
         stop
      endif
      if (hc2.le.0.0D+0) then
         call error(label)
         write(*,*) 'hc2=',hc2,' W.m⁻².K⁻¹'
         write(*,*) 'should be positive'
         stop
      endif
      if (Trad_ref.le.0.0D+0) then
         call error(label)
         write(*,*) 'Trad_ref=',Trad_ref,' K'
         write(*,*) 'should be positive'
         stop
      else
         hr2=4.0D+0*Pl*Trad_ref**3.0D+0 ! linearized radiative exchange coefficient [W.m⁻².K⁻¹]
      endif
      if (Df.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Df=',Df,' m³.s⁻¹'
         write(*,*) 'should be positive or null'
         stop
      endif
      if (Tf0.le.0.0D+0) then
         call error(label)
         write(*,*) 'Tf0=',Tf0,' K'
         write(*,*) 'should be positive'
         stop
      endif
      if (Tf2.le.0.0D+0) then
         call error(label)
         write(*,*) 'Tf2=',Tf2,' K'
         write(*,*) 'should be positive'
         stop
      endif
      if (Tf3.le.0.0D+0) then
         call error(label)
         write(*,*) 'Tf3=',Tf3,' K'
         write(*,*) 'should be positive'
         stop
      endif
      if (Trad.le.0.0D+0) then
         call error(label)
         write(*,*) 'Trad=',Trad,' K'
         write(*,*) 'should be positive'
         stop
      endif
      if (Nmesh.lt.2) then
         call error(label)
         write(*,*) 'Nmesh=',Nmesh
         write(*,*) 'should be greater than 1'
         stop
      endif
      if (Ntime.lt.1) then
         call error(label)
         write(*,*) 'Ntime=',Ntime
         write(*,*) 'should be greater than 0'
         stop
      endif
      if (Ntime.gt.Ntime_mx) then
         call error(label)
         write(*,*) 'Ntime=',Ntime
         write(*,*) '> Ntime_mx=',Ntime_mx
         stop
      endif
      if (alpha.le.0.0D+0) then
         call error(label)
         write(*,*) 'alpha=',alpha
         write(*,*) 'should be positive'
         stop
      endif
      if (total_time.lt.0.0D+0) then
         call error(label)
         write(*,*) 'total_time=',total_time,' s'
         write(*,*) 'should be positive of null'
         stop
      endif
      if (Nsample.le.0) then
         call error(label)
         write(*,*) 'Nsample=',Nsample
         write(*,*) 'should be greater than 0'
         stop
      endif
      if (ddf.le.0) then
         call error(label)
         write(*,*) 'ddf=',ddf
         write(*,*) 'should be greater than 0'
         stop
      endif

      return
      end

