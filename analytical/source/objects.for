c     Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
      subroutine sphere_obj(dim,radius,Ntheta,Nphi,Nv,Nf,vertices,faces)
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'size_params.inc'
c
c     Purpose: to produce a sphere as a wavefront object
c
c     Input:
c       + dim: dimension of space
c       + Ntheta: number of latitude intervals (from pole to pole)
c       + Nphi: number of longitude intervals (from 0 to 2*pi)
c       + radius: radius [m]
c
c     Output:
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: vertices
c       + faces: faces
c
c     I/O
      integer dim
      integer Ntheta,Nphi
      double precision radius
      integer Nv,Nf
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:Nvinface)
c     temp
      double precision r,theta,phi
      double precision dtheta,dphi
      integer itheta,iphi,iv,if,i
      integer i1,i2,i3,i4
      integer Nv_required,Nf_required
c     label
      character*(Nchar_mx) label
      label='subroutine sphere_obj'

      Nv=0
      Nf=0
      r=radius
      dtheta=pi/Ntheta
      dphi=2.0D+0*pi/Nphi

      Nv_required=(Ntheta-1)*Nphi+2
      Nf_required=2*(Ntheta-1)*Nphi
      if (Nv_required.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Required number of vertices:',Nv_required
         write(*,*) '> Nv_so_mx=',Nv_so_mx
         stop
      endif
      if (Nf_required.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Required number of vertices:',Nf_required
         write(*,*) '> Nf_so_mx=',Nf_so_mx
         stop
      endif

c     first point: south pole
      Nv=Nv+1
      if (Nv.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nv_so_mx has been reached'
         stop
      endif
      vertices(Nv,1)=0.0D+0
      vertices(Nv,2)=0.0D+0
      vertices(Nv,3)=-r
c     then for each latitude interval
      do itheta=1,Ntheta-1
         theta=-pi/2.0D+0+itheta*dtheta
         do iphi=1,Nphi
            phi=iphi*dphi
c     vertices
            Nv=Nv+1
            if (Nv.gt.Nv_so_mx) then
               call error(label)
               write(*,*) 'Nv_so_mx has been reached'
               stop
            endif
            vertices(Nv,1)=r*dcos(theta)*dcos(phi)
            vertices(Nv,2)=r*dcos(theta)*dsin(phi)
            vertices(Nv,3)=r*dsin(theta)
c     faces
            if (itheta.eq.1) then
               i1=1
               if (iphi.eq.1) then
                  i2=Nphi+1
                  i3=2
               else
                  i2=iphi
                  i3=iphi+1
               endif            ! iphi=1
               Nf=Nf+1
               if (Nf.gt.Nf_so_mx) then
                  call error(label)
                  write(*,*) 'Nf_so_mx has been reached'
                  stop
               endif
               faces(Nf,1)=i1
               faces(Nf,2)=i3
               faces(Nf,3)=i2
            else
               if (iphi.eq.1) then
                  i1=(itheta-2)*Nphi+Nphi+1
                  i2=(itheta-2)*Nphi+iphi+1
                  i3=(itheta-1)*Nphi+Nphi+1
                  i4=(itheta-1)*Nphi+iphi+1
               else
                  i1=(itheta-2)*Nphi+iphi
                  i2=(itheta-2)*Nphi+iphi+1
                  i3=(itheta-1)*Nphi+iphi
                  i4=(itheta-1)*Nphi+iphi+1
               endif ! iphi=1
               Nf=Nf+1
               if (Nf.gt.Nf_so_mx) then
                  call error(label)
                  write(*,*) 'Nf_so_mx has been reached'
                  stop
               endif
               faces(Nf,1)=i1
               faces(Nf,2)=i2
               faces(Nf,3)=i4
               Nf=Nf+1
               if (Nf.gt.Nf_so_mx) then
                  call error(label)
                  write(*,*) 'Nf_so_mx has been reached'
                  stop
               endif
               faces(Nf,1)=i1
               faces(Nf,2)=i4
               faces(Nf,3)=i3
            endif ! itheta=1
         enddo ! iphi
      enddo ! itheta
c     last point: north pole
      Nv=Nv+1
      if (Nv.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nv_so_mx has been reached'
         stop
      endif
      vertices(Nv,1)=0.0D+0
      vertices(Nv,2)=0.0D+0
      vertices(Nv,3)=r
c     faces
      do iphi=1,Nphi
         Nf=Nf+1
         if (Nf.gt.Nf_so_mx) then
            call error(label)
            write(*,*) 'Nf_so_mx has been reached'
            stop
         endif
         if (iphi.eq.1) then
            i1=(Ntheta-1)*Nphi+1
            i2=(Ntheta-2)*Nphi+2
         else
            i1=(Ntheta-2)*Nphi+iphi
            i2=(Ntheta-2)*Nphi+iphi+1
         endif ! iphi=1
         i3=(Ntheta-1)*Nphi+2
         faces(Nf,1)=i1
         faces(Nf,2)=i2
         faces(Nf,3)=i3
      enddo ! iphi

      if (Nv.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nv=',Nv
         write(*,*) '> Nv_so_mx=',Nv_so_mx
         stop
      endif
      if (Nf.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf=',Nf
         write(*,*) '> Nf_so_mx=',Nf_so_mx
         stop
      endif

      return
      end


      
      subroutine partial_sphere_obj(dim,r,
     &     theta_min,theta_max,
     &     phi_min,phi_max,
     &     Ntheta,Nphi,
     &     Nv,Nf,v,f)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
      include 'param.inc'
c
c     Purpose: to produce a partial sphere as a wavefront object
c
c     Input:
c       + dim: dimension of space
c       + r: radius of the sphere
c       + theta_min: lower value for latitude (-pi/2, pi/2) [rad]
c       + theta_max: higher value for latitude (-pi/2, pi/2) [rad]
c       + phi_min: lower value for longitude [rad]
c       + phi_max: higher value for longitude [rad]
c       + Ntheta: number of latitude intervals (from theta_min to theta_max)
c       + Nphi: number of longitude intervals (from 0 to 2*pi)
c
c     Output:
c       + Nv: number of vertices
c       + Nf: number of faces
c       + v: vertices
c       + f: faces
c
c     I/O
      integer dim
      double precision r
      double precision theta_min,theta_max
      double precision phi_min,phi_max
      integer Ntheta,Nphi
      integer Nv,Nf
      double precision v(1:Nv_so_mx,1:Ndim_mx)
      integer f(1:Nf_so_mx,1:Nvinface)
c     temp=
      double precision theta,phi
      double precision dtheta,dphi
      integer itheta,iphi,iv,iface,i
      integer i1,i2,i3,i4
      logical south_pole,north_pole
c     label
      character*(Nchar_mx) label
      label='subroutine partial_sphere_obj'
c
      if (theta_min.lt.-pi/2.0D+0) then
         call error(label)
         write(*,*) 'theta_min=',theta_min
         write(*,*) '< -pi/2'
         stop
      endif
      if (theta_max.gt.pi/2.0D+0) then
         call error(label)
         write(*,*) 'theta_max=',theta_max
         write(*,*) '> pi/2'
         stop
      endif
      if (theta_max.lt.theta_min) then
         call error(label)
         write(*,*) 'theta_max=',theta_max
         write(*,*) '< theta_min=',theta_min
         stop
      endif
      if (phi_max.lt.phi_min) then
         call error(label)
         write(*,*) 'phi_max=',phi_max
         write(*,*) '< phi_min=',phi_min
         stop
      endif
c
      if (dabs(theta_min+pi/2.0D+0).lt.1.0D-6) then
         south_pole=.true.
      else
         south_pole=.false.
      endif
      if (dabs(theta_max-pi/2.0D+0).lt.1.0D-6) then
         north_pole=.true.
      else
         north_pole=.false.
      endif

c     Angular discretization
      dtheta=(theta_max-theta_min)/dble(Ntheta)
      dphi=(phi_max-phi_min)/dble(Nphi)
c     Vertices
      Nv=0
      if (south_pole) then
c     first point: south pole
         Nv=Nv+1
         if (Nv.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nv_so_mx has been reached'
            stop
         endif
         v(Nv,1)=0.0D+0
         v(Nv,2)=0.0D+0
         v(Nv,3)=-r
      else ! no south pole
c     first set of "Nphi" points
         theta=theta_min
         do iphi=0,Nphi
            phi=phi_min+iphi*dphi
            Nv=Nv+1
            if (Nv.gt.Nv_so_mx) then
               call error(label)
               write(*,*) 'Nv_so_mx has been reached'
               stop
            endif
            v(Nv,1)=r*dcos(theta)*dcos(phi)
            v(Nv,2)=r*dcos(theta)*dsin(phi)
            v(Nv,3)=r*dsin(theta)
         enddo ! iphi
      endif ! south_pole or not
      do itheta=1,Ntheta-1
         theta=theta_min+itheta*dtheta
         do iphi=0,Nphi
            phi=phi_min+iphi*dphi
            Nv=Nv+1
            if (Nv.gt.Nv_so_mx) then
               call error(label)
               write(*,*) 'Nv_so_mx has been reached'
               stop
            endif
            v(Nv,1)=r*dcos(theta)*dcos(phi)
            v(Nv,2)=r*dcos(theta)*dsin(phi)
            v(Nv,3)=r*dsin(theta)
         enddo ! iphi
      enddo ! itheta
c     last point: north pole
      if (north_pole) then
         Nv=Nv+1
         if (Nv.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nv_so_mx has been reached'
            stop
         endif
         v(Nv,1)=0.0D+0
         v(Nv,2)=0.0D+0
         v(Nv,3)=r
      else ! no north pole
c     first set of "Nphi" points
         theta=theta_max
         do iphi=0,Nphi
            phi=phi_min+iphi*dphi
c     vertices
            Nv=Nv+1
            if (Nv.gt.Nv_so_mx) then
               call error(label)
               write(*,*) 'Nv_so_mx has been reached'
               stop
            endif
            v(Nv,1)=r*dcos(theta)*dcos(phi)
            v(Nv,2)=r*dcos(theta)*dsin(phi)
            v(Nv,3)=r*dsin(theta)
         enddo ! iphi
      endif ! north_pole or not
      if (Nv.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nv=',Nv
         write(*,*) '> Nv_so_mx=',Nv_so_mx
         stop
      endif

c     Faces
      Nf=0
      do itheta=1,Ntheta-1
         theta=theta_min+itheta*dtheta
         do iphi=1,Nphi
            if (itheta.eq.1) then
               if (south_pole) then
                  i1=1
                  i2=iphi+1
                  i3=iphi+2
                  Nf=Nf+1
                  if (Nf.gt.Nf_so_mx) then
                     call error(label)
                     write(*,*) 'Nf_so_mx has been reached'
                     stop
                  endif
                  f(Nf,1)=i1
                  f(Nf,2)=i3
                  f(Nf,3)=i2
               else             ! no south pole                  
                  i1=iphi
                  i2=iphi+1
                  i3=Nphi+iphi+1
                  i4=Nphi+iphi+2
                  Nf=Nf+1
                  if (Nf.gt.Nf_so_mx) then
                     call error(label)
                     write(*,*) 'Nf_so_mx has been reached'
                     stop
                  endif
                  f(Nf,1)=i1
                  f(Nf,2)=i2
                  f(Nf,3)=i4
                  Nf=Nf+1
                  if (Nf.gt.Nf_so_mx) then
                     call error(label)
                     write(*,*) 'Nf_so_mx has been reached'
                     stop
                  endif
                  f(Nf,1)=i1
                  f(Nf,2)=i4
                  f(Nf,3)=i3
               endif ! south_pole or not
            else ! itheta>1
               if (south_pole) then
                  i1=(itheta-2)*(Nphi+1)+iphi+1
                  i2=(itheta-2)*(Nphi+1)+iphi+2
                  i3=(itheta-1)*(Nphi+1)+iphi+1
                  i4=(itheta-1)*(Nphi+1)+iphi+2
               else ! no south pole
                  i1=(itheta-1)*(Nphi+1)+iphi
                  i2=(itheta-1)*(Nphi+1)+iphi+1
                  i3=(itheta)*(Nphi+1)+iphi
                  i4=(itheta)*(Nphi+1)+iphi+1
               endif            ! south_pole or not
               Nf=Nf+1
               if (Nf.gt.Nf_so_mx) then
                  call error(label)
                  write(*,*) 'Nf_so_mx has been reached'
                  stop
               endif
               f(Nf,1)=i1
               f(Nf,2)=i2
               f(Nf,3)=i4
               Nf=Nf+1
               if (Nf.gt.Nf_so_mx) then
                  call error(label)
                  write(*,*) 'Nf_so_mx has been reached'
                  stop
               endif
               f(Nf,1)=i1
               f(Nf,2)=i4
               f(Nf,3)=i3
            endif               ! itheta=1
         enddo ! iphi
      enddo ! itheta
      do iphi=1,Nphi
         if (north_pole) then
            if (south_pole) then
               i1=(Ntheta-2)*(Nphi+1)+iphi+1
               i2=(Ntheta-2)*(Nphi+1)+iphi+2
               i3=(Ntheta-1)*(Nphi+1)+2
            else ! no south pole
               i1=(Ntheta-1)*(Nphi+1)+iphi
               i2=(Ntheta-1)*(Nphi+1)+iphi+1
               i3=Ntheta*(Nphi+1)+1
            endif ! south pole or not
            Nf=Nf+1
            if (Nf.gt.Nf_so_mx) then
               call error(label)
               write(*,*) 'Nf_so_mx has been reached'
               stop
            endif
            f(Nf,1)=i1
            f(Nf,2)=i2
            f(Nf,3)=i3
         else ! no north pole
            if (south_pole) then
               i1=(Ntheta-2)*(Nphi+1)+iphi+1
               i2=i1+1
               i3=(Ntheta-1)*(Nphi+1)+iphi+1
               i4=i3+1
            else                ! no south pole
               i1=(Ntheta-1)*(Nphi+1)+iphi
               i2=(Ntheta-1)*(Nphi+1)+iphi+1
               i3=Ntheta*(Nphi+1)+iphi
               i4=Ntheta*(Nphi+1)+iphi+1
            endif               ! south pole or not
            Nf=Nf+1
            if (Nf.gt.Nf_so_mx) then
               call error(label)
               write(*,*) 'Nf_so_mx has been reached'
               stop
            endif
            f(Nf,1)=i1
            f(Nf,2)=i2
            f(Nf,3)=i4
            Nf=Nf+1
            if (Nf.gt.Nf_so_mx) then
               call error(label)
               write(*,*) 'Nf_so_mx has been reached'
               stop
            endif
            f(Nf,1)=i1
            f(Nf,2)=i4
            f(Nf,3)=i3
         endif ! north pole or not
      enddo ! iphi

      if (Nf.gt.Nf_mx) then
         call error(label)
         write(*,*) 'Nf=',Nf
         write(*,*) '> Nf_mx=',Nf_mx
         stop
      endif

      return
      end
