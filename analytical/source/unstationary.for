      program unstationary
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'formats.inc'
c
c     Purpose: to evaluate the unstationary temperatures
c     of the fluid and the solid in the case of a spherical shell
c     composed of a arbitrary large number of different materials, with interface thermal resistances
c
c     Input data files:
c       + data.in
c       + fluids.in
c       + solids.in
c       + thermal_resistances.in
c       + probe_positions.in
c
c     This version computes:
c       + The temperature of the internal fluid, of boundaries, of junctions (on both sides when the
c     thermal resistance is positive) and for each discretized position in every solid, but also records
c     the temporal evolution of the temperature for every probe position declared in the "probe_positions.in" file.
c     For a arbitrary position in a solid, the temperature is linearly interpolated from temperatures
c     computed at discretized positions.
c
c     The program also produces within the "resuts/stardis_files" folder: the "model.txt" thermal model file
c     for Stardis, and the 'run.sh' script for running Stardis computations at required probe positions.
c
c     Finally, the program will produce the "gra_T" gnuplot script file that is used by the "plot_Ts5"
c     script in order to compare analytical and Stardis results.
c
c     data
      character*(Nchar_mx) data_file
      double precision R1
      double precision hc1,hc2,hr2
      double precision Df
      double precision Tf0
      double precision Tf2,Tf3
      double precision Trad
      double precision Trad_ref
      integer Nmesh
      integer Ntime
      double precision alpha
      double precision total_time
      integer Nsample
      double precision ddf
      integer Nfluid
      double precision rho_fluid(1:Nfluid_mx)
      double precision C_fluid(1:Nfluid_mx)
      integer Nsolid
      double precision e_solid(1:Nsolid_mx)
      double precision lambda_solid(1:Nsolid_mx)
      double precision rho_solid(1:Nsolid_mx)
      double precision C_solid(1:Nsolid_mx)
      double precision P_solid(1:Nsolid_mx)
      double precision Tinit_solid(1:Nsolid_mx)
      integer Ntr
      double precision R(1:Nsolid_mx)
      integer Nprobe
      double precision probe_position(1:Nprobe_mx,1:Ndim_mx)
c     temp
      integer dim,i,j,it,isolid,solid_idx,idx,idx_eq
      double precision S1,V1
      double precision dx(1:Nsolid_mx)
      double precision alpha_solid(1:Nsolid_mx)
      double precision t
      integer Neq,iprobe
      character*(Nchar_mx) results_file
      double precision Tp1(0:Ntime_mx),Tp2(0:Ntime_mx)
      double precision Tj(1:Ntr_mx,0:Ntime_mx)
      double precision Tj_inf(1:Ntr_mx)
      double precision Tj1(1:Ntr_mx,0:Ntime_mx)
      double precision Tj2(1:Ntr_mx,0:Ntime_mx)
      double precision Tj1_inf(1:Ntr_mx)
      double precision Tj2_inf(1:Ntr_mx)
      double precision Tp1_inf,Tp2_inf
      double precision diff,sum_e,R2
      character*(Nchar_mx) comment
      character*10 isolid_str,i_str
      character*3 iprobe_str
      double precision x(1:Ndim_mx),d
      logical solid_found,is_junction
      double precision T1,T2,Ts,x1,x2
      character*(Nchar_mx) command
c     LAPACK I/O
      integer info,lda,ldb,nrhs
      integer, dimension(:), allocatable :: ipiv
      double precision, dimension(:), allocatable :: vecB_orig
      double precision, dimension(:), allocatable :: vecB
      double precision, dimension(:,:), allocatable :: matA_orig
      double precision, dimension(:,:), allocatable :: matA
      double precision, dimension(:,:), allocatable :: matEV
      double precision, dimension(:), allocatable :: Coeff,sol
      double precision, dimension(:,:), allocatable :: Temp,Temp2
      double precision, dimension(:), allocatable :: Tinit,Tinf,lambda
      character*1 jobvl,jobvr,uplo
      integer ldvl,ldvr,lwork
      double precision, dimension(:,:), allocatable :: VL
      double precision, dimension(:,:), allocatable :: VR
      double precision, dimension(:), allocatable :: wr
      double precision, dimension(:), allocatable :: wi
      double precision, dimension(:), allocatable :: work
c     label
      character*(Nchar_mx) label
      label='program unstationary'

      dim=3                     ! dimension of space
      command='rm -f ./results/temperature*.txt'
      call exec(command)
      command='rm -f ./results/stardis_files/stardis_result_*.txt'
      call exec(command)
c     -----------------------------------------------------------------------
c     Reading input data
c     -----------------------------------------------------------------------
      data_file='./data.in'
      call read_data(data_file,R1,hc1,hc2,hr2,Df,Tf0,Tf2,Tf3,Trad,Trad_ref,Nmesh,Ntime,alpha,total_time,Nsample,ddf)
      data_file='./fluids.in'
      call read_fluid_properties(data_file,Nfluid,rho_fluid,C_fluid)
      if (Nfluid.ne.3) then
         call error(label)
         write(*,*) 'Nfluid=',Nfluid
         write(*,*) 'is supposed to be equal to 3'
         stop
      endif
      data_file='./solids.in'
      call read_solid_properties(data_file,Nsolid,e_solid,lambda_solid,rho_solid,C_solid,P_solid,Tinit_solid)
      data_file='./thermal_resistances.in'
      call read_thermal_resistances(data_file,Ntr,R)
      if (Ntr.ne.Nsolid-1) then
         call error(label)
         write(*,*) 'Number of thermal resistances:',Ntr
         write(*,*) 'should be equal to Nsolid-1=',Nsolid-1
         stop
      endif
      data_file='./probe_positions.in'
      call read_probe_positions(data_file,dim,Nprobe,probe_position)
      sum_e=0.0D+0
      do isolid=1,Nsolid
         sum_e=sum_e+e_solid(isolid)
      enddo                     ! isolid
      R2=R1+sum_e
      do iprobe=1,Nprobe
         do j=1,dim
            x(j)=probe_position(iprobe,j)
         enddo                  ! j
         call vector_length(dim,x,d)
         if (d.gt.R1+sum_e) then
            call error(label)
            write(*,*) 'Probe position index ',iprobe,'=',(x(j),j=1,dim)
            write(*,*) 'distance to center=',d,' m'
            write(*,*) 'should be < ',R1+sum_e,' m'
            stop
         endif
      enddo                     ! irpobe
c     Produce Stardis model and script files
      call stardis_files(dim,R1,hc1,hc2,hr2,Df,Tf0,Tf2,Tf3,Trad,Trad_ref,Ntime,alpha,total_time,Nsample,ddf,
     &     Nfluid,rho_fluid,C_fluid,
     &     Nsolid,e_solid,lambda_solid,rho_solid,C_solid,P_solid,Tinit_solid,Ntr,R,Nprobe,probe_position)
c     Produce gnuplot script
      call gnuplot_files(dim,R1,Nsolid,e_solid,Ntr,R,Nprobe,probe_position,Nsample)
c
      S1=4.0D+0*pi*R1**2.0D+0        ! [m²]
      V1=4.0D+0*pi/3.0D+0*R1**3.0D+0 ! [m³]
      do isolid=1,Nsolid
         dx(isolid)=e_solid(isolid)/dble(Nmesh)                                 ! [m]
         alpha_solid(isolid)=lambda_solid(isolid)/(rho_solid(isolid)*C_solid(isolid)) ! [m²/s]
      enddo                     ! isolid
      Neq=Nsolid*Nmesh+1
c     
      allocate(matA_orig(1:Neq,1:Neq))
      allocate(matA(1:Neq,1:Neq))
      allocate(matEV(1:Neq,1:Neq))
      allocate(vecB_orig(1:Neq))
      allocate(vecB(1:Neq))
      allocate(Coeff(1:Neq))
      allocate(Tinit(1:Neq))
      allocate(Tinf(1:Neq))
      allocate(Temp(1:Neq,0:Ntime_mx))
      allocate(Temp2(1:Neq,0:Ntime_mx))
      allocate(sol(1:Neq))
      allocate(lambda(1:Neq))
      allocate(ipiv(1:Neq))
      allocate(VL(1:Neq,1:Neq))
      allocate(VR(1:Neq,1:Neq))
      allocate(wi(1:Neq))
      allocate(wr(1:Neq))
c     Initialization
      do i=1,Neq
         do j=1,Neq
            matA_orig(i,j)=0.0D+0
         enddo                  ! j
      enddo                     ! i
c
c     -----------------------------------------------------------------------
c     Matrix A
c     -----------------------------------------------------------------------
      nrhs=1
      lda=Neq
      ldb=Neq
c     d[Tf,1(t)]/dt
      matA_orig(1,1)=(S1*hc1*(hc1/(hc1+2.0D+0*lambda_solid(1)/dx(1))-1.0D+0)-rho_fluid(3)*C_fluid(3)*Df)/(rho_fluid(1)*C_fluid(1)*V1)
      matA_orig(1,2)=(2.0D+0*S1*hc1*lambda_solid(1)/dx(1))/((hc1+2.0D+0*lambda_solid(1)/dx(1))*rho_fluid(1)*C_fluid(1)*V1)
      do isolid=1,Nsolid
c     d[Ts,isolid,1(t)]/dt
         if (isolid.eq.1) then
            matA_orig((isolid-1)*Nmesh+2,(isolid-1)*Nmesh+1)=2.0D+0*alpha_solid(1)/(dx(1)**2.0D+0)*hc1/(hc1+2.0D+0*lambda_solid(1)/dx(1))
            matA_orig((isolid-1)*Nmesh+2,(isolid-1)*Nmesh+2)=2.0D+0*alpha_solid(1)/(dx(1)**2.0D+0)*((2.0D+0*lambda_solid(1)/dx(1))/(hc1+2.0D+0*lambda_solid(1)/dx(1))-1.5D+0)
            matA_orig((isolid-1)*Nmesh+2,(isolid-1)*Nmesh+3)=alpha_solid(1)/(dx(1)**2.0D+0)
         else
            if (R(isolid-1).eq.0.0D+0) then
               matA_orig((isolid-1)*Nmesh+2,(isolid-1)*Nmesh+1)=2.0D+0*alpha_solid(isolid)/(dx(isolid)**2.0D+0)*(lambda_solid(isolid-1)/dx(isolid-1))/(lambda_solid(isolid-1)/dx(isolid-1)+lambda_solid(isolid)/dx(isolid))
            else
               matA_orig((isolid-1)*Nmesh+2,(isolid-1)*Nmesh+1)=2.0D+0*alpha_solid(isolid)/(dx(isolid)**2.0D+0)*2.0D+0*lambda_solid(isolid-1)*R(isolid-1)/dx(isolid-1)
     &              /((1.0D+0+2.0D+0*lambda_solid(isolid-1)*R(isolid-1)/dx(isolid-1))*(1.0D+0+2.0D+0*lambda_solid(isolid)*R(isolid-1)/dx(isolid))-1.0D+0)
            endif
            if (R(isolid-1).eq.0.0D+0) then
               matA_orig((isolid-1)*Nmesh+2,(isolid-1)*Nmesh+2)=2.0D+0*alpha_solid(isolid)/(dx(isolid)**2.0D+0)*((lambda_solid(isolid)/dx(isolid))/(lambda_solid(isolid-1)/dx(isolid-1)+lambda_solid(isolid)/dx(isolid))-1.50D+0)
            else
               matA_orig((isolid-1)*Nmesh+2,(isolid-1)*Nmesh+2)=2.0D+0*alpha_solid(isolid)/(dx(isolid)**2.0D+0)*(2.0D+0*lambda_solid(isolid)*R(isolid-1)/dx(isolid)*(1.0D+0+2.0D+0*lambda_solid(isolid-1)*R(isolid-1)/dx(isolid-1))
     &              /((1.0D+0+2.0D+0*lambda_solid(isolid-1)*R(isolid-1)/dx(isolid-1))*(1.0D+0+2.0D+0*lambda_solid(isolid)*R(isolid-1)/dx(isolid))-1.0D+0)-1.50D+0)
            endif
            matA_orig((isolid-1)*Nmesh+2,(isolid-1)*Nmesh+3)=alpha_solid(isolid)/dx(isolid)**2.0D+0
         endif                  ! isolid=1 or not
c     d[Ts,isolid,i(t)]/dt
         do i=(isolid-1)*Nmesh+3,isolid*Nmesh
            matA_orig(i,i-1)=alpha_solid(isolid)/(dx(isolid)**2.0D+0)
            matA_orig(i,i)=-2.0D+0*alpha_solid(isolid)/(dx(isolid)**2.0D+0)
            matA_orig(i,i+1)=alpha_solid(isolid)/(dx(isolid)**2.0D+0)
         enddo                  ! i
c     d[Tsolid,isolid,N(t)]/dt
         if (isolid.lt.Nsolid) then
            matA_orig(isolid*Nmesh+1,isolid*Nmesh)=alpha_solid(isolid)/dx(isolid)**2.0D+0
            if (R(isolid).eq.0.0D+0) then
               matA_orig(isolid*Nmesh+1,isolid*Nmesh+1)=2.0D+0*alpha_solid(isolid)/(dx(isolid)**2.0D+0)*((lambda_solid(isolid)/dx(isolid))/(lambda_solid(isolid)/dx(isolid)+lambda_solid(isolid+1)/dx(isolid+1))-1.50D+0)
            else
               matA_orig(isolid*Nmesh+1,isolid*Nmesh+1)=2.0D+0*alpha_solid(isolid)/(dx(isolid)**2.0D+0)*(2.0D+0*lambda_solid(isolid)*R(isolid)/dx(isolid)*(1.0D+0+2.0D+0*lambda_solid(isolid+1)*R(isolid)/dx(isolid+1))
     &              /((1.0D+0+2.0D+0*lambda_solid(isolid)*R(isolid)/dx(isolid))*(1.0D+0+2.0D+0*lambda_solid(isolid+1)*R(isolid)/dx(isolid+1))-1.0D+0)-1.50D+0)
            endif
            if (R(isolid).eq.0.0D+0) then
               matA_orig(isolid*Nmesh+1,isolid*Nmesh+2)=2.0D+0*alpha_solid(isolid)/(dx(isolid)**2.0D+0)*(lambda_solid(isolid+1)/dx(isolid+1))/(lambda_solid(isolid)/dx(isolid)+lambda_solid(isolid+1)/dx(isolid+1))
            else
               matA_orig(isolid*Nmesh+1,isolid*Nmesh+2)=2.0D+0*alpha_solid(isolid)/(dx(isolid)**2.0D+0)*2.0D+0*lambda_solid(isolid+1)*R(isolid)/dx(isolid+1)
     &              /((1.0D+0+2.0D+0*lambda_solid(isolid)*R(isolid)/dx(isolid))*(1.0D+0+2.0D+0*lambda_solid(isolid+1)*R(isolid)/dx(isolid+1))-1.0D+0)
            endif
         else
            matA_orig(isolid*Nmesh+1,isolid*Nmesh)=alpha_solid(Nsolid)/(dx(Nsolid)**2.0D+0)
            matA_orig(isolid*Nmesh+1,isolid*Nmesh+1)=2.0D+0*alpha_solid(Nsolid)/(dx(Nsolid)**2.0D+0)*((2.0D+0*lambda_solid(Nsolid)/dx(Nsolid))/(2.0D+0*lambda_solid(Nsolid)/dx(Nsolid)+hc2+hr2)-1.5D+0)
         endif
      enddo                     ! isolid
c     -----------------------------------------------------------------------
c     Vector B
c     -----------------------------------------------------------------------
      vecB_orig(1)=-rho_fluid(3)*C_fluid(3)*Df/(rho_fluid(1)*C_fluid(1)*V1)*Tf3
      do isolid=1,Nsolid
         if (isolid.lt.Nsolid) then
            do i=(isolid-1)*Nmesh+2,isolid*Nmesh+1
               vecB_orig(i)=-P_solid(isolid)/(rho_solid(isolid)*C_solid(isolid))
            enddo               ! i
         else
            do i=(isolid-1)*Nmesh+2,isolid*Nmesh
               vecB_orig(i)=-P_solid(isolid)/(rho_solid(isolid)*C_solid(isolid))
            enddo               ! i
            vecB_orig(isolid*Nmesh+1)=-(2.0D+0*alpha_solid(Nsolid)/(dx(Nsolid)**2.0D+0)*(hc2*Tf2+hr2*Trad)/(hc2+hr2+2.0D+0*lambda_solid(Nsolid)/dx(Nsolid))+P_solid(Nsolid)/(rho_solid(Nsolid)*C_solid(Nsolid)))
         endif                  ! isolid=Nsolid or not
      enddo                     ! isolid
c     Tinit
      Tinit(1)=Tf0
      do isolid=1,Nsolid
         do i=(isolid-1)*Nmesh+2,isolid*Nmesh+1
            Tinit(i)=Tinit_solid(isolid)
         enddo                  ! i
      enddo                     ! isolid

c     Debug
      open(21,file='./results/matrix.txt')
      write(21,*) 'Mat(A) / Vec(B)'
      do i=1,Neq
         write(21,51) i,(matA_orig(i,j),j=1,Neq),vecB_orig(i)
      enddo                     ! i
      close(21)
c     Debug
c     
c     -----------------------------------------------------------------------
c     Stationary solution (@ t->inf)
c     -----------------------------------------------------------------------
      uplo='a'
      call DLACPY(uplo,Neq,Neq,matA_orig,lda,matA,lda)
      call DLACPY(uplo,Neq,1,vecB_orig,ldb,vecB,ldb)
      call DGESV(Neq,nrhs,matA,lda,ipiv,vecB,ldb,info)
      if (info.ne.0) then
         call error(label)
         write(*,*) 'while solving stationary system'
         stop
      else
         do i=1,Neq
            Tinf(i)=vecB(i)
         enddo                  ! i
c     Temperature of boundary 1 (on the cavity side) @ stationary
         Tp1_inf=hc1/(hc1+2.0D+0*(lambda_solid(1)/dx(1)))*Tinf(1)
     &        +2.0D+0*(lambda_solid(1)/dx(1))/(hc1+2.0D+0*lambda_solid(1)/dx(1))*Tinf(2)
c     Temperature of boundary 2 (on external side) @ stationary
         Tp2_inf=2.0D+0*lambda_solid(Nsolid)/dx(Nsolid)
     &        /(2.0D+0*lambda_solid(Nsolid)/dx(Nsolid)+hc2+hr2)*Tinf(Neq)
     &        +hc2/(2.0D+0*lambda_solid(Nsolid)/dx(Nsolid)+hc2+hr2)*Tf2
     &        +hr2/(2.0D+0*lambda_solid(Nsolid)/dx(Nsolid)+hc2+hr2)*Trad
c     Temperature of junction @ stationary
         do isolid=1,Nsolid-1
            if (R(isolid).eq.0.0D+0) then
               Tj_inf(isolid)=(lambda_solid(isolid)/dx(isolid)*Tinf(Nmesh+1)+lambda_solid(isolid+1)/dx(isolid+1)*Tinf(Nmesh+2))/(lambda_solid(isolid)/dx(isolid)+lambda_solid(isolid+1)/dx(isolid+1))
            else
               Tj1_inf(isolid)=(2.0D+0*lambda_solid(isolid)*R(isolid)/dx(isolid)*(1.0D+0+2.0D+0*lambda_solid(isolid+1)*R(isolid)/dx(isolid+1))*Tinf(Nmesh+1)+2.0D+0*lambda_solid(isolid+1)*R(isolid)/dx(isolid+1)*Tinf(Nmesh+2))
     &              /((1.0D+0+2.0D+0*lambda_solid(isolid)*R(isolid)/dx(isolid))*(1.0D+0+2.0D+0*lambda_solid(isolid+1)*R(isolid)/dx(isolid+1))-1.0D+0)
               Tj2_inf(isolid)=(2.0D+0*lambda_solid(isolid)*R(isolid)/dx(isolid)*Tinf(Nmesh+1)+2.0D+0*lambda_solid(isolid+1)*R(isolid)/dx(isolid+1)*(1.0D+0+2.0D+0*lambda_solid(isolid)*R(isolid)/dx(isolid))*Tinf(Nmesh+2))
     &              /((1.0D+0+2.0D+0*lambda_solid(isolid)*R(isolid)/dx(isolid))*(1.0D+0+2.0D+0*lambda_solid(isolid+1)*R(isolid)/dx(isolid+1))-1.0D+0)
            endif
         enddo                  ! i
      endif
c      
c     -----------------------------------------------------------------------
c     Unstationary solution (@ t)
c     -----------------------------------------------------------------------
c
c     Solve eigenvalues and eigenvectors
      jobvl='V'
      jobvr='V'
      ldvl=Neq
      ldvr=Neq
      lwork=4*Neq
      allocate(work(lwork))
      uplo='a'
      call DLACPY(uplo,Neq,Neq,matA_orig,lda,matA,lda)
      call DGEEV(jobvl,jobvr,Neq,matA,lda,wr,wi,VL,ldvl,VR,ldvr,work,lwork,info)
      if (info.ne.0) then
         call error(label)
         write(*,*) 'while solving eigenvalues'
         stop
      endif
      do i=1,Neq
         if (wi(i).ne.0.0D+0) then
            call error(label)
            write(*,*) 'wi(',i,')=',wi(i)
            stop
         else
            lambda(i)=wr(i)
         endif
      enddo ! i
      do i=1,Neq
         do j=1,Neq
            matEV(i,j)=VR(i,j)/VR(1,j)
         enddo ! j
      enddo ! i
      uplo='a'
      call DLACPY(uplo,Neq,Neq,matEV,lda,matA,lda)
      do i=1,Neq
         vecB(i)=Tinit(i)-Tinf(i)
      enddo ! i
      call DGESV(Neq,nrhs,matA,lda,ipiv,vecB,ldb,info)
      if (info.ne.0) then
         call error(label)
         write(*,*) 'while solving coefficients'
         stop
      else
         do i=1,Neq
            Coeff(i)=vecB(i)
         enddo ! i
      endif
      deallocate(work)
      deallocate(matA)
      deallocate(vecB)

c     The solution consists in an array of functions:
c     (Tf,1(t), Tm,1(t), Tm,2(t), ..., Tm,N(t))
c     with Tf,1(t) the temperature of the fluid in the cavity
c     and Tm,i(t) the temperature at the center of the i-th slab of solid
c
      results_file='./results/temperatures.txt'
      open(11,file=trim(results_file))
      comment='# t [s] / Tf,1(t) [K] / Tp,1(t) [K] /'
      do isolid=1,Nsolid-1
         call num2str(isolid,isolid_str)
         do i=1,Nmesh
            call num2str(i,i_str)
            comment=trim(comment)//' Ts,'//trim(isolid_str)//','//trim(i_str)//'(t) [K] /'
         enddo                  ! i
         if (R(isolid).eq.0.0D+0) then
            comment=trim(comment)//' Tjunction('//trim(isolid_str)//')(t) [K] /'
         else
            comment=trim(comment)//' Tjunction1('//trim(isolid_str)//')(t) [K] / Tjunction2('//trim(isolid_str)//')(t) [K] /'
         endif
      enddo                     ! isolid
      call num2str(Nsolid,isolid_str)
      do i=1,Nmesh
         call num2str(i,i_str)
         comment=trim(comment)//' Ts,'//trim(isolid_str)//','//trim(i_str)//'(t) [K] /'
      enddo                     ! i
      comment=trim(comment)//' Tp2(t) [K] / Difference [K]'
      write(11,10) trim(comment)
      do it=0,Ntime
         call time(it,Ntime,alpha,total_time,t)
         do i=1,Neq
            Temp(i,it)=Tinf(i)
            Temp2(i,it)=Tinf(i)
            do j=1,Neq
               Temp(i,it)=Temp(i,it)+matEV(i,j)*Coeff(j)*dexp(lambda(j)*t)
               Temp2(i,it)=Temp2(i,it)+matEV(i,j)*Coeff(j)*dexp(lambda(j)*(t+1.D+0))
            enddo               ! j
c     Debug
            if (Temp(i,it).lt.0.0D+0) then
               call error(label)
               write(*,*) 'Temp(',i,',',it,')=',Temp(i,it)
               write(*,*) 'matEV / Coeff / lambda*t'
               do j=1,Neq
                  write(*,*) matEV(i,j),Coeff(j),lambda(j)*t
               enddo            ! j
               stop
            endif
c     Debug
         enddo                  ! i
c     Temperature of boundary 1 (on the cavity side)
         if (t.eq.0.0D+0) then
            Tp1(it)=hc1/(hc1+2.0D+0*(lambda_solid(1)/dx(1)))*Tf0+2.0D+0*(lambda_solid(1)/dx(1))/(hc1+2.0D+0*lambda_solid(1)/dx(1))*Tinit_solid(1)
         else
            Tp1(it)=hc1/(hc1+2.0D+0*(lambda_solid(1)/dx(1)))*Temp(1,it)+2.0D+0*(lambda_solid(1)/dx(1))/(hc1+2.0D+0*lambda_solid(1)/dx(1))*Temp(2,it)
         endif
c     Temperature of boundary 2 (on external side)
         if (t.eq.0.0D+0) then
            Tp2(it)=2.0D+0*lambda_solid(Nsolid)/dx(Nsolid)/(2.0D+0*lambda_solid(Nsolid)/dx(Nsolid)+hc2+hr2)*Tinit_solid(Nsolid)
     &           +hc2/(2.0D+0*lambda_solid(Nsolid)/dx(Nsolid)+hc2+hr2)*Tf2
     &           +hr2/(2.0D+0*lambda_solid(Nsolid)/dx(Nsolid)+hc2+hr2)*Trad
         else
            Tp2(it)=2.0D+0*lambda_solid(Nsolid)/dx(Nsolid)/(2.0D+0*lambda_solid(Nsolid)/dx(Nsolid)+hc2+hr2)*Temp(Neq,it)
     &           +hc2/(2.0D+0*lambda_solid(Nsolid)/dx(Nsolid)+hc2+hr2)*Tf2
     &           +hr2/(2.0D+0*lambda_solid(Nsolid)/dx(Nsolid)+hc2+hr2)*Trad
         endif
         do isolid=1,Nsolid-1
c     Temperature of junction
            if (R(isolid).eq.0.0D+0) then
               Tj(isolid,it)=(lambda_solid(isolid)/dx(isolid)*Temp(Nmesh+1,it)+lambda_solid(isolid+1)/dx(isolid+1)*Temp(Nmesh+2,it))/(lambda_solid(isolid)/dx(isolid)+lambda_solid(isolid+1)/dx(isolid+1))
            else
               Tj1(isolid,it)=(2.0D+0*lambda_solid(isolid)*R(isolid)/dx(isolid)*(1.0D+0+2.0D+0*lambda_solid(isolid+1)*R(isolid)/dx(isolid+1))*Temp(Nmesh+1,it)+2.0D+0*lambda_solid(isolid+1)*R(isolid)/dx(isolid+1)*Temp(Nmesh+2,it))
     &              /((1.0D+0+2.0D+0*lambda_solid(isolid)*R(isolid)/dx(isolid))*(1.0D+0+2.0D+0*lambda_solid(isolid+1)*R(isolid)/dx(isolid+1))-1.0D+0)
               Tj2(isolid,it)=(2.0D+0*lambda_solid(isolid)*R(isolid)/dx(isolid)*Temp(Nmesh+1,it)+2.0D+0*lambda_solid(isolid+1)*R(isolid)/dx(isolid+1)*(1.0D+0+2.0D+0*lambda_solid(isolid)*R(isolid)/dx(isolid))*Temp(Nmesh+2,it))
     &              /((1.0D+0+2.0D+0*lambda_solid(isolid)*R(isolid)/dx(isolid))*(1.0D+0+2.0D+0*lambda_solid(isolid+1)*R(isolid)/dx(isolid+1))-1.0D+0)
            endif
         enddo                  ! isolid
         diff=0.0D+0
         do i=1,Neq
            sol(i)=0.0D+0
            do j=1,Neq
               sol(i)=sol(i)+matA_orig(i,j)*Temp(j,it)
            enddo               ! j
            sol(i)=sol(i)-vecB_orig(i)
            diff=diff+dabs(sol(i)-(Temp2(i,it)-Temp(i,it)))
         enddo                  ! i
c     record
         if (t.eq.0.0D+0) then
            write(11,50,advance='no') t,Tf0,Tp1(it)
         else
            write(11,50,advance='no') t,Temp(1,it),Tp1(it)
         endif
         do isolid=1,Nsolid-1
            if (t.eq.0.0D+0) then
               write(11,50,advance='no') (Tinit_solid(isolid),i=(isolid-1)*Nmesh+2,isolid*Nmesh+1)
            else
               write(11,50,advance='no') (Temp(i,it),i=(isolid-1)*Nmesh+2,isolid*Nmesh+1)
            endif
            if (R(isolid).eq.0.0D+0) then
               if (t.eq.0.0D+0) then
                  write(11,50,advance='no') Tinit_solid(isolid)
               else
                  write(11,50,advance='no') Tj(isolid,it)
               endif
            else
               if (t.eq.0.0D+0) then
                  write(11,50,advance='no') Tinit_solid(isolid)
                  write(11,50,advance='no') Tinit_solid(isolid+1)
               else
                  write(11,50,advance='no') Tj1(isolid,it)
                  write(11,50,advance='no') Tj2(isolid,it)
               endif
            endif
         enddo                  ! isolid
         if (t.eq.0.0D+0) then
            write(11,50) (Tinit_solid(Nsolid),i=(Nsolid-1)*Nmesh+2,Neq),Tp2(it),diff
         else
            write(11,50) (Temp(i,it),i=(Nsolid-1)*Nmesh+2,Neq),Tp2(it),diff
         endif
      enddo                     ! it
      diff=0.0D+0
      do i=1,Neq
         sol(i)=0.0D+0
         do j=1,Neq
            sol(i)=sol(i)+matA_orig(i,j)*Tinf(j)
         enddo                  ! j
         sol(i)=sol(i)-vecB_orig(i)
         diff=diff+dabs(sol(i))
      enddo                     ! i
      deallocate(matEV)
      deallocate(matA_orig)
      deallocate(vecB_orig)
      deallocate(Coeff)
      deallocate(Tinit)
      deallocate(sol)
      deallocate(lambda)
      deallocate(ipiv)
      deallocate(VL)
      deallocate(VR)
      deallocate(wi)
      deallocate(wr)
      
c     record
      write(11,50,advance='no') t/0.0,Tinf(1),Tp1_inf
      do isolid=1,Nsolid-1
         write(11,50,advance='no') (Tinf(i),i=(isolid-1)*Nmesh+2,isolid*Nmesh+1)
         if (R(isolid).eq.0.0D+0) then
            write(11,50,advance='no') Tj_inf(isolid)
         else
            write(11,50,advance='no') Tj1_inf(isolid)
            write(11,50,advance='no') Tj2_inf(isolid)
         endif
      enddo                     ! isolid
      write(11,50,advance='no') (Tinf(i),i=(Nsolid-1)*Nmesh+2,Neq),Tp2_inf,diff
c
      sum_e=0.0D+0
      write(11,*)
      write(11,*) '#'
      write(11,*) '# Note: values of x [m]'
      write(11,*) '# Tp,1: ',R1
      do isolid=1,Nsolid-1
         do i=1,Nmesh
            write(11,*) '# Ts,',isolid,',',i,':',R1+sum_e+dx(isolid)*(dble(i)-0.5D+0)
         enddo                  ! i
         sum_e=sum_e+e_solid(isolid)
         write(11,*) '# Tjunction:',R1+sum_e
      enddo                     ! isolid
      do i=1,Nmesh
         write(11,*) '# Ts,',Nsolid,',',i,':',R1+sum_e+dx(Nsolid)*(dble(i)-0.5D+0)
      enddo                     ! i
      write(11,*) '# Tp,2:',R1+sum_e+e_solid(Nsolid)
      close(11)
      write(*,*) 'Output file was recorded: ',trim(results_file)
c
      do iprobe=1,Nprobe
         call num2str3(iprobe,iprobe_str)
         results_file='./results/temperature_position'//trim(iprobe_str)//'.txt'
         do j=1,dim
            x(j)=probe_position(iprobe,j)
         enddo                  ! j
         call vector_length(dim,x,d)
         open(12,file=trim(results_file))
c     Debug
c         write(*,*) 'File opened: ',trim(results_file)
c     Debug
         if (d.lt.R1) then      ! temperature of internal fluid
            write(12,10) '# Temperature of internal fluid'
            write(12,10) '# time [s] / Tfluid(t) [K]'
            do it=0,Ntime
               call time(it,Ntime,alpha,total_time,t)
               if (t.eq.0.0D+0) then
                  write(12,50) t,Tf0
               else
                  write(12,50) t,Temp(1,it)
               endif
            enddo               ! it
            write(12,50) t/0.0D+0,Tinf(1)
         else if (d.eq.R1) then ! temperature of internal boundary
            write(12,10) '# Temperature of internal boundary'
            write(12,10) '# time [s] / Tp1(t) [K]'
            do it=0,Ntime
               call time(it,Ntime,alpha,total_time,t)
               write(12,50) t,Tp1(it)
            enddo               ! it
            write(12,50) t/0.0D+0,Tp1_inf
         else if (d.eq.R2) then ! temperature of external boundary
            write(12,10) '# Temperature of external boundary'
            write(12,10) '# time [s] / Tp2(t) [K]'
            do it=0,Ntime
               call time(it,Ntime,alpha,total_time,t)
               write(12,50) t,Tp2(it)
            enddo               ! it
            write(12,50) t/0.0D+0,Tp2_inf
         else
c     looking for solid index
            solid_found=.false.
            is_junction=.false.
            sum_e=0.0D+0
            do isolid=1,Nsolid
               if ((d.gt.R1+sum_e).and.(d.lt.R1+sum_e+e_solid(isolid))) then
                  solid_found=.true.
                  solid_idx=isolid
                  idx_eq=Nmesh*(solid_idx-1)+1
                  idx=int((d-R1-sum_e-dx(solid_idx)/2.0D+0)/dx(solid_idx))+1
                  if (idx.eq.0) then
                     x1=R1+sum_e
                  else
                     x1=R1+sum_e+dx(solid_idx)*(dble(idx)-0.5D+0)
                  endif
                  if (idx.eq.Nmesh) then
                     x2=R1+sum_e+e_solid(solid_idx)
                  else
                     x2=R1+sum_e+dx(solid_idx)*(dble(idx+1)-0.5D+0)
                  endif
c     Debug
                  if ((idx.lt.0).or.(idx.gt.Nmesh)) then
                     call error(label)
                     write(*,*) 'solid_idx=',solid_idx
                     write(*,*) 'd=',d
                     write(*,*) 'idx=',idx
                     write(*,*) 'Out of bounds:',0,Nmesh
                     stop
                  endif
                  if ((x1.lt.R1+sum_e).or.(x1.gt.R1+sum_e+e_solid(solid_idx))) then
                     call error(label)
                     write(*,*) 'x1=',x1
                     write(*,*) 'solid_idx=',solid_idx
                     write(*,*) 'Limits:',R1+sum_e,R1+sum_e+e_solid(solid_idx)
                     stop
                  endif
                  if ((x2.lt.R1+sum_e).or.(x2.gt.R1+sum_e+e_solid(solid_idx))) then
                     call error(label)
                     write(*,*) 'x2=',x2
                     write(*,*) 'solid_idx=',solid_idx
                     write(*,*) 'Limits:',R1+sum_e,R1+sum_e+e_solid(solid_idx)
                     stop
                  endif
                  if ((d.lt.x1).or.(d.gt.x2)) then
                     call error(label)
                     write(*,*) 'solid_idx=',solid_idx
                     write(*,*) 'd=',d
                     write(*,*) 'idx=',idx
                     write(*,*) 'should be in [',x1,', ',x2,'] range'
                     stop
                  endif
c     "idx" is the index of the interval in solid "solid_idx" where the probe position is located
c     "idx_eq" is the index if the solution array
c     Debug
                  goto 111
               endif
               if (d.eq.R1+sum_e+e_solid(isolid)) then
                  solid_found=.true.
                  solid_idx=isolid
                  is_junction=.true.
                  goto 111
               endif
               sum_e=sum_e+e_solid(isolid)
            enddo               ! isolid
 111        continue
            if (.not.solid_found) then
               call error(label)
               write(*,*) 'Solid could not be identified for probe position:',(x(j),j=1,dim)
               write(*,*) 'Distance to center=',d,' m'
               stop
            endif
c     Debug
c            write(*,*) 'solid_found=',solid_found
c            if (solid_found) then
c               write(*,*) 'solid_idx=',solid_idx
c               write(*,*) 'is_junction=',is_junction
c               write(*,*) 'idx=',idx
c               write(*,*) 'd=',d
c               write(*,*) 'x1=',x1,' x2=',x2
c            endif
c     Debug
c     file records
            if (is_junction) then ! temperature(s) of the junction
               if (R(solid_idx).eq.0.0D+0) then
                  write(12,43) '# Temperature of junction ',solid_idx
                  write(12,44) '# time [s] / Tj',solid_idx,'(t) [K]'
                  do it=0,Ntime
                     call time(it,Ntime,alpha,total_time,t)
                     if (t.eq.0.0D+0) then
                        write(12,50) t,Tinit_solid(solid_idx)
                     else
                        write(12,50) t,Tj(solid_idx,it)
                     endif
                  enddo         ! it
                  write(12,50) t/0.0D+0,Tj_inf(solid_idx)
               else
                  write(12,43) '# Temperatures of junction ',solid_idx
                  write(12,45) '# time [s] / Tj1',solid_idx,'(t) [K] / Tj2',solid_idx,'(t) [K]'
                  do it=0,Ntime
                     call time(it,Ntime,alpha,total_time,t)
                     if (t.eq.0.0D+0) then
                        write(12,50) t,Tinit_solid(solid_idx),Tinit_solid(solid_idx+1)
                     else
                        write(12,50) t,Tj1(solid_idx,it),Tj2(solid_idx,it)
                     endif
                  enddo         ! it
                  write(12,50) t/0.0D+0,Tj1_inf(solid_idx),Tj2_inf(solid_idx)
               endif            ! R(solid_idx)
            else                ! temperature in solid
               write(12,46) '# Temperature in solid index',solid_idx,' @ x=',(x(j),j=1,dim)
               write(12,44) '# time [s] / Ts',solid_idx,'(t) [K]'
               do it=0,Ntime
                  call time(it,Ntime,alpha,total_time,t)
                  if (t.eq.0.0D+0) then
                     write(12,50) t,Tinit_solid(solid_idx)
                  else
                     if (idx.eq.0) then
                        if (solid_idx.eq.1) then
                           T1=Tp1(it)
                        else
                           if (R(solid_idx).eq.0.0D+0) then
                              T1=Tj(solid_idx,it)
                           else
                              T1=Tj2(solid_idx,it)
                           endif ! R(solid_idx)
                        endif   ! solid_idx
                        T2=Temp(idx_eq+1,it)
                     else if (idx.eq.Nmesh) then
                        T1=Temp(idx_eq+Nmesh,it)
                        if (solid_idx.eq.Nsolid) then
                           T2=Tp2(it)
                        else
                           if (R(solid_idx).eq.0.0D+0) then
                              T2=Tj(solid_idx,it)
                           else
                              T2=Tj1(solid_idx,it)
                           endif ! R(solid_idx)
                        endif   ! solid_idx
                     else
                        T1=Temp(idx_eq+idx,it)
c     Debug
                        if (T1.lt.0.0D+0) then
                           call error(label)
                           write(*,*) 'T1=',T1
                           write(*,*) 'idx_eq=',idx_eq
                           write(*,*) 'idx=',idx
                           write(*,*) 'it=',it
                           stop
                        endif
c     Debug
                        T2=Temp(idx_eq+idx+1,it)
                     endif
c     Debug
                     if (T1.lt.0.0D+0) then
                        call error(label)
                        write(*,*) 'T1=',T1
                        write(*,*) 'idx=',idx
                        stop
                     endif
c     Debug
                     Ts=T1+(T2-T1)/(x2-x1)*(d-x1)
c     Debug
                     if (Ts.lt.0.0D+0) then
                        call error(label)
                        write(*,*) 'Ts=',Ts
                        write(*,*) 'T1=',T1
                        write(*,*) 'T2=',T2
                        write(*,*) 'x1=',x1,' x2=',x2,' d=',d
                        stop
                     endif
c     Debug
                     write(12,50) t,Ts
                  endif         ! t=0
               enddo            ! it
               if (idx.eq.0) then
                  if (solid_idx.eq.1) then
                     T1=Tp1_inf
                  else
                     if (R(solid_idx).eq.0.0D+0) then
                        T1=Tj_inf(solid_idx)
                     else
                        T1=Tj2_inf(solid_idx)
                     endif      ! R(solid_idx)
                  endif         ! solid_idx
                  T2=Tinf(idx_eq+1)
               else if (idx.eq.Nmesh) then
                  T1=Tinf(idx_eq+Nmesh)
                  if (solid_idx.eq.Nsolid) then
                     T2=Tp2_inf
                  else
                     if (R(solid_idx).eq.0.0D+0) then
                        T2=Tj_inf(solid_idx)
                     else
                        T2=Tj1_inf(solid_idx)
                     endif      ! R(solid_idx)
                  endif         ! solid_idx
               else
                  T1=Tinf(idx_eq+idx)
                  T2=Tinf(idx_eq+idx+1)
               endif
               Ts=T1+(T2-T1)/(x2-x1)*(d-x1)
               write(12,50) t/0.0D+0,Ts
            endif               ! is_junction
         endif                  ! d < or > R1
         close(12)
         write(*,*) 'Output file was recorded: ',trim(results_file)
      enddo                     ! irpobe
      deallocate(Tinf)
      deallocate(Temp)
      deallocate(Temp2)
      command='rm -rf ./nlines*'
      call exec(command)
      
      end
