      subroutine read_probe_positions(data_file,dim,Nprobe,probe_position)
      include 'max.inc'
c     
c     Purpose: to read input solid thermophysical properties
c     
c     Input:
c       + data_file: file to read
c       + dim: dimension of space
c     
c     Output:
c       + Nprobe: number of probe positions
c       + probe_position: probe positions [m, m, m]
c     
c     I/O
      character*(Nchar_mx) data_file
      integer dim
      integer Nprobe
      double precision probe_position(1:Nprobe_mx,1:Ndim_mx)
c     temp
      integer file_ios,line_ios,i
      logical keep_going
      double precision tmp(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine read_probe_positions'

      open(11,file=trim(data_file),status='old',iostat=file_ios)
      if (file_ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(data_file)
         stop
      endif                     ! file_ios
      do i=1,3
         read(11,*)
      enddo                     ! i
      Nprobe=0
      keep_going=.true.
      do while (keep_going)
         read(11,*,iostat=line_ios) (tmp(j),j=1,dim)
         if (line_ios.ne.0) then
            keep_going=.false.
         else
            Nprobe=Nprobe+1
            if (Nprobe.gt.Nprobe_mx) then
               call error(label)
               write(*,*) 'Nprobe_mx has been reached'
               stop
            endif
            do j=1,dim
               probe_position(Nprobe,j)=tmp(j)
            enddo               ! j
         endif                  ! line_ios
      enddo                     ! while (keep_going)
      close(11)
      if (Nprobe.le.0) then
         call error(label)
         write(*,*) 'Nprobe=',Nprobe
         write(*,*) 'should be > 0'
         stop
      endif

      return
      end
