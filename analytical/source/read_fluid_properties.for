      subroutine read_fluid_properties(data_file,Nfluid,rho_fluid,C_fluid)
      include 'max.inc'
c     
c     Purpose: to read input fluid thermophysical properties
c     
c     Input:
c       + data_file: file to read
c     
c     Output:
c       + Nfluid: number of fluids
c       + rho_fluid: volumic mass [kg.m⁻³]
c       + C_fluid: massic specific heat [J.kg⁻1K⁻¹]
c     
c     I/O
      character*(Nchar_mx) data_file
      integer Nfluid
      double precision rho_fluid(1:Nfluid_mx)
      double precision C_fluid(1:Nfluid_mx)
c     temp
      integer file_ios,line_ios,i
      logical keep_going
      double precision tmp1,tmp2
c     label
      character*(Nchar_mx) label
      label='subroutine read_fluid_properties'

      open(11,file=trim(data_file),status='old',iostat=file_ios)
      if (file_ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(data_file)
         stop
      endif
      do i=1,3
         read(11,*)
      enddo                     ! i
      Nfluid=0
      keep_going=.true.
      do while (keep_going)
         read(11,*,iostat=line_ios) tmp1,tmp2
         if (line_ios.ne.0) then
            keep_going=.false.
         else
            Nfluid=Nfluid+1
            if (Nfluid.gt.Nfluid_mx) then
               call error(label)
               write(*,*) 'Nfluid_mx has been reached'
               stop
            endif
            rho_fluid(Nfluid)=tmp1
            if (rho_fluid(Nfluid).le.0.0D+0) then
               call error(label)
               write(*,*) 'rho_fluid(',Nfluid,')=',rho_fluid(Nfluid)
               write(*,*) 'should be > 0'
               stop
            endif
            C_fluid(Nfluid)=tmp2
            if (C_fluid(Nfluid).le.0.0D+0) then
               call error(label)
               write(*,*) 'C_fluid(',Nfluid,')=',C_fluid(Nfluid)
               write(*,*) 'should be > 0'
               stop
            endif
         endif                  ! line_ios
      enddo                     ! while (keep_going)
      close(11)
      if (Nfluid.le.0) then
         call error(label)
         write(*,*) 'Nfluid=',Nfluid
         write(*,*) 'should be > 0'
         stop
      endif

      return
      end
