      subroutine gnuplot_files(dim,R1,Nsolid,e_solid,Ntr,R,Nprobe,probe_position,Nsample)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to produce the gnuplot script that makes possible to compare
c     analytical and Stardis results
c     
c     Input:
c       + dim: dimension of space
c       + R1: radius of the internal cavity [m]
c       + Nsolid: number of solids
c       + e_solid: thickness [m]
c       + Ntr: numher of thermal resistances
c       + R: thermal resistances [m².K.W⁻¹]
c       + Nprobe: number of probe positions
c       + probe_position: probe positions [m, m, m]
c       + Nsample: number of samples for Stardis
c     
c     Output:
c       + the "gra_T" gnuplot script
c     
c     I/O
      integer dim
      double precision R1
      integer Nsolid
      double precision e_solid(1:Nsolid_mx)
      integer Ntr
      double precision R(1:Nsolid_mx)
      integer Nprobe
      double precision probe_position(1:Nprobe_mx,1:Ndim_mx)
      integer Nsample
c     temp
      character*(Nchar_mx) gnuplot_script
      integer iprobe,j,nap,isolid
      double precision x(1:Ndim_mx),d
      double precision sum_e,R2
      character*3 iprobe_str
      character*10 Nsample_str
      character*10 solid_str
      character*(Nchar_mx) stardis_file
      character*(Nchar_mx) stardis_file1
      character*(Nchar_mx) stardis_file2
      character*(Nchar_mx) dstr,ylabel_line
      logical err,solid_found,is_junction
      integer solid_idx,Nfile
c     label
      character*(Nchar_mx) label
      label='subroutine gnuplot_files'

      nap=4
      sum_e=0.0D+0              ! [m]
      do isolid=1,Nsolid
         sum_e=sum_e+e_solid(isolid) ! [m]
      enddo                     ! isolid
      R2=R1+sum_e               ! [m]
      call num2str(Nsample,Nsample_str)

      gnuplot_script='./results/gra_T'
      open(11,file=trim(gnuplot_script))
      write(11,10) 'set term pdf enhanced color font "helvetica,20" linewidth 1 size 10,6'
      write(11,10) 'set output "T.pdf"'
      write(11,10) 'set xlabel "t [s]"'
      write(11,10) 'set key top left'
      do iprobe=1,Nprobe
         Nfile=1
         call num2str3(iprobe,iprobe_str)
         do j=1,dim
            x(j)=probe_position(iprobe,j)
         enddo                  ! j
         call vector_length(dim,x,d)
         call dble2str_noexp(d,nap,dstr,err)
         if (err) then
            call error(label)
            write(*,*) 'Could not convert to string: d=',d
            stop
         endif
c     looking for solid index
         if (d.lt.R1) then
            solid_idx=0         ! code for internal fluid
         else if (d.eq.R1) then
            solid_idx=1
         else if (d.eq.R2) then
            solid_idx=Nsolid
         else            
            solid_found=.false.
            is_junction=.false.
            sum_e=0.0D+0
            do isolid=1,Nsolid
               if ((d.gt.R1+sum_e).and.(d.lt.R1+sum_e+e_solid(isolid))) then
                  solid_found=.true.
                  solid_idx=isolid
                  goto 111
               endif
               if (d.eq.R1+sum_e+e_solid(isolid)) then
                  solid_found=.true.
                  solid_idx=isolid
                  is_junction=.true.
                  goto 111
               endif
               sum_e=sum_e+e_solid(isolid)
            enddo               ! isolid
 111        continue
            if (.not.solid_found) then
               call error(label)
               write(*,*) 'Solid could not be identified for probe position:',(x(j),j=1,dim)
               write(*,*) 'Distance to center=',d,' m'
               stop
            endif
            if ((is_junction).and.(R(solid_idx).gt.0.0D+0)) then
               Nfile=2
            endif
         endif                  ! d
         call num2str(isolid,solid_str)
c     
         if (Nfile.eq.1) then
            stardis_file='./stardis_files/stardis_result_N'//trim(Nsample_str)//'_position'//trim(iprobe_str)//'.txt'
            if (d.lt.R1) then
               ylabel_line='set ylabel "Tfluid [K]"'
            else if (d.eq.R1) then
               ylabel_line='set ylabel "Tint. boundary [K]"'
            else if (d.eq.R2) then
               ylabel_line='set ylabel "Text. boundary [K]"'
            else
               if (is_junction) then
                  ylabel_line='set ylabel "Tjunction'//trim(solid_str)//' [K]"'
               else
                  ylabel_line='set ylabel "Ts(d='//trim(dstr)//'m) [K]"'
               endif
            endif
            write(11,10) trim(ylabel_line)
            write(11,10) 'plot \\'
            write(11,10) '     "'//trim(stardis_file)//'" u 1:2:3 title "Stardis" w errorbars lt -1 lw 1 \\'
            write(11,10) '    ,"./temperature_position'//trim(iprobe_str)//'.txt" u 1:2 title "analytical" w points lt 1 lw 1 \\'
            write(11,*)
         else
            stardis_file1='./stardis_files/stardis_result_N'//trim(Nsample_str)//'_position'//trim(iprobe_str)//'_1.txt'
            ylabel_line='set ylabel "Tjunction'//trim(solid_str)//' in solid '//trim(solid_str)//' [K]"'
            write(11,10) trim(ylabel_line)
            write(11,10) 'plot \\'
            write(11,10) '     "'//trim(stardis_file1)//'" u 1:2:3 title "Stardis" w errorbars lt -1 lw 1 \\'
            write(11,10) '    ,"./temperature_position'//trim(iprobe_str)//'.txt" u 1:2 title "analytical" w points lt 1 lw 1 \\'
            write(11,*)
            call num2str(isolid+1,solid_str)
            stardis_file2='./stardis_files/stardis_result_N'//trim(Nsample_str)//'_position'//trim(iprobe_str)//'_2.txt'
            ylabel_line='set ylabel "Tjunction'//trim(solid_str)//' in solid '//trim(solid_str)//' [K]"'
            write(11,10) trim(ylabel_line)
            write(11,10) 'plot \\'
            write(11,10) '     "'//trim(stardis_file2)//'" u 1:2:3 title "Stardis" w errorbars lt -1 lw 1 \\'
            write(11,10) '    ,"./temperature_position'//trim(iprobe_str)//'.txt" u 1:3 title "analytical" w points lt 1 lw 1 \\'
            write(11,*)
         endif
      enddo                     ! iprobe
         
      write(11,10) 'quit'
      close(11)
      write(*,*) 'File was recorded: ',trim(gnuplot_script)
      
      return
      end
