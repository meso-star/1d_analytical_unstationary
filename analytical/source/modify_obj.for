c     Copyright (C) 2021 |Meso|Star> (contact@meso-star.com)
      subroutine duplicate_so(dim,Nv_so,Nf_so,v_so,f_so,Nv_so2,Nf_so2,v_so2,f_so2)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c
c     Purpose: to duplicate a single object
c
c     Input:
c       + dim: dimension of space
c       + Nv_so: number of vertices in original object
c       + Nf_so: number of faces in original object
c       + v_so: vertices in original object
c       + f_so: faces in original object
c
c     Output:
c       + Nv_so2: number of vertices in duplicated object
c       + Nf_so2: number of faces in duplicated object
c       + v_so2: vertices in duplicated object
c       + f_so2: faces in duplicated object
c
c     I/O
      integer dim
      integer Nv_so,Nf_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      integer f_so(1:Nf_so_mx,1:Nvinface)
      integer Nv_so2,Nf_so2
      double precision v_so2(1:Nv_so_mx,1:Ndim_mx)
      integer f_so2(1:Nf_so_mx,1:Nvinface)
c     temp
      integer ivertex,iface,j
c     label
      character*(Nchar_mx) label
      label='subroutine duplicate_so'

      Nv_so2=Nv_so
      do ivertex=1,Nv_so
         do j=1,dim
            v_so2(ivertex,j)=v_so(ivertex,j)
         enddo                  ! j
      enddo                     ! ivertex
      Nf_so2=Nf_so
      do iface=1,Nf_so
         do j=1,Nvinface
            f_so2(iface,j)=f_so(iface,j)
         enddo                  ! j
      enddo                     ! iface

      return
      end

      
      
      subroutine check_vertex_indexes(Nf_so,f_so)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c
c     Purpose: to check for inconsistencies in the vertex indexes
c     for each face
c
c     Input:
c       + Nf_so: number of faces
c       + f_so: faces
c
c     Output:
c       + f_so: updated
c
c     I/O
      integer Nf_so
      integer f_so(1:Nf_so_mx,1:Nvinface)
c     temp
      integer if
c     label
      character*(Nchar_mx) label
      label='subroutine check_vertex_indexes'

      do if=1,Nf_so
         if (f_so(if,1).eq.f_so(if,2)) then
            call error(label)
            write(*,*) 'f_so(',if,',1)=',f_so(if,1)
            write(*,*) 'f_so(',if,',2)=',f_so(if,2)
            stop
         endif
         if (f_so(if,1).eq.f_so(if,3)) then
            call error(label)
            write(*,*) 'f_so(',if,',1)=',f_so(if,1)
            write(*,*) 'f_so(',if,',3)=',f_so(if,3)
            stop
         endif
         if (f_so(if,2).eq.f_so(if,3)) then
            call error(label)
            write(*,*) 'f_so(',if,',2)=',f_so(if,2)
            write(*,*) 'f_so(',if,',3)=',f_so(if,3)
            stop
         endif
      enddo ! i

      return
      end


      
      subroutine rotate_obj(dim,Nv_so,v_so,M)
      implicit none
      include 'max.inc'
c
c     Purpose: to rotate an object
c
c     Input:
c       + dim: dimension of space
c       + Nv_so: number of vertices
c       + v_so: vertices
c       + M: rotation matrix
c
c     Output:
c       + v_so: updated
c
c     I/O
      integer dim
      integer Nv_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer iv,i
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine rotate_obj'

      do iv=1,Nv_so
         do i=1,dim
            x1(i)=v_so(iv,i)
         enddo ! i
         call matrix_vector(dim,M,x1,x2)
         do i=1,dim
            v_so(iv,i)=x2(i)
         enddo ! i
      enddo ! iv

      return
      end



      subroutine move_obj(dim,Nv_so,v_so,center)
      implicit none
      include 'max.inc'
c
c     Purpose: to move an object
c
c     Input:
c       + dim: dimension of space
c       + Nv_so: number of vertices
c       + v_so: vertices
c       + center: cartesian coordinates of the new center
c
c     Output:
c       + v_so: updated
c
c     I/O
      integer dim
      integer Nv_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      double precision center(1:Ndim_mx)
c     temp
      integer iv,i
c     label
      character*(Nchar_mx) label
      label='subroutine move_obj'

      do iv=1,Nv_so
         do i=1,dim
            v_so(iv,i)=v_so(iv,i)+center(i)
         enddo ! i
      enddo ! iv

      return
      end



      subroutine scale_obj(dim,Nv_so,v_so,scale)
      implicit none
      include 'max.inc'
c
c     Purpose: to scale an object
c
c     Input:
c       + dim: dimension of space
c       + Nv_so: number of vertices
c       + v_so: vertices
c       + scale: scaling ratio
c
c     Output:
c       + v_so: updated
c
c     I/O
      integer dim
      integer Nv_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      double precision scale
c     temp
      integer iv,i
c     label
      character*(Nchar_mx) label
      label='subroutine scale_obj'

      if (scale.le.0.0D+0) then
         call error(label)
         write(*,*) 'scale=',scale
         write(*,*) 'should be positive'
         stop
      endif

      do iv=1,Nv_so
         do i=1,dim
            v_so(iv,i)=v_so(iv,i)*scale
         enddo ! i
      enddo ! iv

      return
      end


      
      subroutine invert_normals(Nf,f01)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: invert the normal for all faces of a given object
c     
c     Input:
c       + Nf: number of faces of the object
c       + f01: definition of faces
c     
c     Output:
c       + f01: updated
c     
c     I/O
      integer Nf
      integer f01(1:Nf_so_mx,1:Nvinface)
c     temp
      integer iface,tmp1
c     label
      character*(Nchar_mx) label
      label='subroutine invert_normals'

      do iface=1,Nf
         tmp1=f01(iface,2)
         f01(iface,2)=f01(iface,3)
         f01(iface,3)=tmp1
      enddo                     ! iface

      return
      end

      
      
      subroutine remove_face_list_from_object(dim,Nf,faces,material_index,Nf_tr,f_tr)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to remove a list of triangular faces from a object
c     
c     Input:
c       + dim: dimension of space
c       + Nf, faces, material_index: definition of the object
c       + Nf_tr,f_tr: list of faces to remove
c     
c     Output:
c       + Nv, Nf, vertices, faces: updated
c     
c     I/O
      integer dim
      integer Nf
      integer faces(1:Nf_mx,1:Nvinface)
      integer material_index(1:Nf_mx)
      integer Nf_tr
      integer f_tr(1:Nf_so_mx)
c     temp
      logical face_sorted(1:Nf_mx)
      integer f_tr_sorted(1:Nf_mx)
      integer iface,face_index,i,j,iface_max,idx_face_max
c     label
      character*(Nchar_mx) label
      label='subroutine remove_face_list_from_object'

c     Sort by descending order
      do i=1,Nf_tr
         face_sorted(i)=.false.
      enddo                     ! iface
      do i=1,Nf_tr
         iface_max=0
         do iface=1,Nf_tr
            if ((f_tr(iface).gt.iface_max).and.(.not.face_sorted(iface))) then
               idx_face_max=iface
               iface_max=f_tr(iface)
            endif
         enddo                  ! iface
         if (iface_max.eq.0) then
            call error(label)
            write(*,*) 'iface_max=',iface_max
            stop
         endif
         f_tr_sorted(i)=iface_max
         face_sorted(idx_face_max)=.true.
      enddo                     ! i
c     Debug
c      do i=1,Nf_tr
c         write(*,*) 'f_tr_sorted(',i,')=',f_tr_sorted(i)
c      enddo                     ! i
c     Debug

      do i=1,Nf_tr
         face_index=f_tr_sorted(i)
         do iface=face_index,Nf-1
            do j=1,Nvinface
               faces(iface,j)=faces(iface+1,j)
            enddo               ! j
            material_index(iface)=material_index(iface+1)
         enddo                  ! iface
         Nf=Nf-1
      enddo                     ! i

      return
      end
      

      
      subroutine add_face_to_object(dim,Nv,Nf,vertices,faces,x1,x2,x3)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to add a triangular face to a object
c     
c     Input:
c       + dim: dimension of space
c       + Nv, Nf, vertices, faces: definition of the object
c       + x1, x2, x3: cartesian coordinates of the 3 nodes that define the face to add
c     
c     Output:
c       + Nv, Nf, vertices, faces: updated
c     
c     I/O
      integer dim
      integer Nv,Nf
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:Nvinface)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
c     temp
      integer iv,iface,j
      double precision d1,d2,d3
      double precision x(1:Ndim_mx)
      logical identical_vertex_found
c     parameters
      double precision d_min
      parameter(d_min=1.0D-3)   ! [m]
c     label
      character*(Nchar_mx) label
      label='subroutine add_face_to_object'
c
      if (Nv.eq.0) then
         Nv=1
         do j=1,dim
            vertices(1,j)=x1(j)
         enddo                  ! j
      endif
c     
      Nf=Nf+1
      if (Nf.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf_so_mx has been reached'
         stop
      endif
c
c     x1
      identical_vertex_found=.false.
      do iv=1,Nv
         do j=1,dim
            x(j)=vertices(iv,j)
         enddo                  ! j
         call distance(dim,x1,x,d1)
         if (d1.le.d_min) then  ! point x1 already exists in the "vertices" list
            faces(Nf,1)=iv
            identical_vertex_found=.true.
            goto 111
         endif
      enddo                     ! iv
 111  continue
      if (.not.identical_vertex_found) then
         Nv=Nv+1
         if (Nv.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nv_so_mx has been reached'
            stop
         endif
         do j=1,dim
            vertices(Nv,j)=x1(j)
         enddo                  ! j
         faces(Nf,1)=Nv
      endif
c     x2
      identical_vertex_found=.false.
      do iv=1,Nv
         do j=1,dim
            x(j)=vertices(iv,j)
         enddo                  ! j
         call distance(dim,x2,x,d2)
         if (d2.le.d_min) then  ! point x2 already exists in the "vertices" list
            faces(Nf,2)=iv
            identical_vertex_found=.true.
            goto 112
         endif
      enddo                     ! iv
 112  continue
      if (.not.identical_vertex_found) then
         Nv=Nv+1
         if (Nv.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nv_so_mx has been reached'
            stop
         endif
         do j=1,dim
            vertices(Nv,j)=x2(j)
         enddo                  ! j
         faces(Nf,2)=Nv
      endif
c     x3
      identical_vertex_found=.false.
      do iv=1,Nv
         do j=1,dim
            x(j)=vertices(iv,j)
         enddo                  ! j
         call distance(dim,x3,x,d3)
         if (d3.le.d_min) then  ! point x2 already exists in the "vertices" list
            faces(Nf,3)=iv
            identical_vertex_found=.true.
            goto 113
         endif
      enddo                     ! iv
 113  continue
      if (.not.identical_vertex_found) then
         Nv=Nv+1
         if (Nv.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nv_so_mx has been reached'
            stop
         endif
         do j=1,dim
            vertices(Nv,j)=x3(j)
         enddo                  ! j
         faces(Nf,3)=Nv
      endif
c     
      return
      end
