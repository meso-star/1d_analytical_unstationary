c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine add2obj(dim
     &     ,Nv,Nf,vertices,faces
     &     ,invert_normal
     &     ,Nv_so,Nf_so,v_so,f_so)
      implicit none
      include 'max.inc'
c
c     Purpose: to add a given object to the total wavefront object
c
c     Input:
c       + dim: dimension of space
c       + Nv, Nf, vertices, faces: definition of the total wavefront object
c       + Nv_so, Nf_so, v_so, f_so: definition of the object to add
c       + invert_normal: true if the direction of normals has to be inverted
c
c     Output:
c       + Nv, Nf, vertices, faces: updated
c
c     I/O
      integer dim
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      logical invert_normal
      integer Nv_so,Nf_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      integer f_so(1:Nf_so_mx,1:3)
c     temp
      integer iv,if,i,tmp1
      integer Nv0
c     label
      character*(Nchar_mx) label
      label='subroutine add2obj'

c     Initial number of vertices in the global list
      Nv0=Nv
c     Add vertices to the global list
      do iv=1,Nv_so
         Nv=Nv+1
         if (Nv.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv=',Nv
            write(*,*) '> Nv_mx=',Nv_mx
            stop
         endif
         do i=1,dim
            vertices(Nv,i)=v_so(iv,i)
         enddo ! i
      enddo ! iv
c     Add faces to the global list
      do if=1,Nf_so
         Nf=Nf+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf=',Nf
            write(*,*) '> Nf_mx=',Nf_mx
            stop
         endif
         faces(Nf,1)=f_so(if,1)+Nv0
         if (invert_normal) then
            faces(Nf,2)=f_so(if,3)+Nv0
            faces(Nf,3)=f_so(if,2)+Nv0
         else
            faces(Nf,2)=f_so(if,2)+Nv0
            faces(Nf,3)=f_so(if,3)+Nv0
         endif
      enddo ! if

      return
      end


      
      subroutine add2sobj(dim
     &     ,Nv,Nf,vertices,faces
     &     ,invert_normal
     &     ,Nv_so,Nf_so,v_so,f_so)
      implicit none
      include 'max.inc'
c
c     Purpose: to add a given object to a single wavefront object
c
c     Input:
c       + dim: dimension of space
c       + Nv, Nf, vertices, faces: definition of the total wavefront object
c       + Nv_so, Nf_so, v_so, f_so: definition of the object to add
c       + invert_normal: true if the direction of normals has to be inverted
c
c     Output:
c       + Nv, Nf, vertices, faces: updated
c
c     I/O
      integer dim
      integer Nv,Nf
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
      logical invert_normal
      integer Nv_so,Nf_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      integer f_so(1:Nf_so_mx,1:3)
c     temp
      integer iv,if,i,tmp1
      integer Nv0
c     label
      character*(Nchar_mx) label
      label='subroutine add2sobj'

c     Initial number of vertices in the global list
      Nv0=Nv
c     Invert normals if needed
      if (invert_normal) then
         call invert_normals(Nf_so,faces)
      endif ! invert_normal
c     Add vertices to the global list
      do iv=1,Nv_so
         Nv=Nv+1
         if (Nv.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nv=',Nv
            write(*,*) '> Nv_so_mx=',Nv_so_mx
            stop
         endif
         do i=1,dim
            vertices(Nv,i)=v_so(iv,i)
         enddo ! i
      enddo ! iv
c     Add faces to the global list
      do if=1,Nf_so
         Nf=Nf+1
         if (Nf.gt.Nf_so_mx) then
            call error(label)
            write(*,*) 'Nf=',Nf
            write(*,*) '> Nf_so_mx=',Nf_so_mx
            stop
         endif
         do i=1,3
            faces(Nf,i)=f_so(if,i)+Nv0
         enddo ! i
      enddo ! if

      return
      end
