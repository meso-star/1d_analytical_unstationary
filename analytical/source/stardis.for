      subroutine stardis_files(dim,R1,hc1,hc2,hr2,Df,Tf0,Tf2,Tf3,Trad,Trad_ref,Ntime,alpha,total_time,Nsample,ddf,
     &     Nfluid,rho_fluid,C_fluid,
     &     Nsolid,e_solid,lambda_solid,rho_solid,C_solid,P_solid,Tinit_solid,Ntr,R,Nprobe,probe_position)
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'size_params.inc'
      include 'formats.inc'
c     
c     Purpose: produce all files required by STARDIS
c     
c     Input:
c       + dim: dimension of space
c       + R1: radius of the internal cavity [m]
c       + hc1: convective exchange coefficient of internal boundary [W/(m^2.K)]
c       + hc2: convective exchange coefficient of external boundary [W/(m^2.K)]
c       + hr2: radiative exchange coefficient of external boundary [W/(m^2.K)]
c       + Df: volumic rate of fluid [m^3/s]
c       + Tf0: initial temperature of the fluid [K]
c       + Tf2: temperature of the external fluid [K]
c       + Tf3: temperature of the replacement fluid [K]
c       + Trad: external radiative exchange temperature [K]
c       + Trad_ref: reference radiative temperature [K]
c       + Ntime: number of time points
c       + alpha: delta(t)_i=alpha*delta(t)_i-1
c       + total_time: final time
c       + Nsample: number of samples for Stardis
c       + ddf: value of delta=e/ddf in Stardis
c       + Nfluid: number of fluids
c       + rho_fluid: volumic mass [kg.m⁻³]
c       + C_fluid: massic specific heat [J.kg⁻1K⁻¹]
c       + Nsolid: number of solids
c       + e_solid: thickness [m]
c       + lambda_solid: conductivity [W.m⁻¹.K⁻¹]
c       + rho_solid: volumic mass [kg.m⁻³]
c       + C_solid: massic specific heat [J.kg⁻1K⁻¹]
c       + P_solid: term source [W.⁻³]
c       + Tinit_solid: initial temperature [K]
c       + Ntr: numher of thermal resistances
c       + R: thermal resistances [m².K.W⁻¹]
c       + Nprobe: number of probe positions
c       + probe_position: probe positions [m, m, m]
c     
c     Output:
c       + .stl files
c       + Stardis model file
c     
c     I/O
      integer dim
      double precision R1
      double precision hc1,hc2,hr2
      double precision Df
      double precision Tf0
      double precision Tf2,Tf3
      double precision Trad
      double precision Trad_ref
      integer Ntime
      double precision alpha
      double precision total_time
      integer Nsample
      double precision ddf
      integer Nfluid
      double precision rho_fluid(1:Nfluid_mx)
      double precision C_fluid(1:Nfluid_mx)
      integer Nsolid
      double precision e_solid(1:Nsolid_mx)
      double precision lambda_solid(1:Nsolid_mx)
      double precision rho_solid(1:Nsolid_mx)
      double precision C_solid(1:Nsolid_mx)
      double precision P_solid(1:Nsolid_mx)
      double precision Tinit_solid(1:Nsolid_mx)
      integer Ntr
      double precision R(1:Nsolid_mx)
      integer Nprobe
      double precision probe_position(1:Nprobe_mx,1:Ndim_mx)
c     temp
      double precision R2,sum_e
      double precision epsilon
      integer Nv_so,Nf_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      integer f_so(1:Nf_so_mx,1:Nvinface)
      integer Nv_unitysphere,Nf_unitysphere
      double precision v_unitysphere(1:Nv_so_mx,1:Ndim_mx)
      integer f_unitysphere(1:Nf_so_mx,1:Nvinface)
      integer Nv,Nf
      double precision v(1:Nv_mx,1:Ndim_mx)
      integer f(1:Nf_mx,1:Nvinface)
      logical invert_normal,normal_out
      character*(Nchar_mx) stl_file,model_file,script_file
      integer Ninterface,interface,isolid,i,itr
      double precision radius,r_in,r_out
      character*(Nchar_mx) interface_name,solid_name,tr_name
      character*(Nchar_mx) line
      character*10 solid_str,tr_str,Nsample_str
      character*(Nchar_mx) obj_file,command
      integer iprobe,j
      character*3 iprobe_str
      character*(Nchar_mx) mat_str
      logical solid_found,is_junction
      integer solid_idx,Nfile
      double precision d
      double precision x(1:Ndim_mx)
      integer nap
      character*(Nchar_mx) time_str,xstr,line1
      logical err
      double precision t
c     label
      character*(Nchar_mx) label
      label='stardis_files'

      epsilon=1.0D-6            ! [m]
      nap=8

      sum_e=0.0D+0              ! [m]
      do isolid=1,Nsolid
         sum_e=sum_e+e_solid(isolid) ! [m]
      enddo                     ! isolid
      R2=R1+sum_e               ! [m]
      
      obj_file='./data/unit_sphere.obj'
      call read_obj(obj_file,dim,Nv_unitysphere,Nf_unitysphere,v_unitysphere,f_unitysphere)
      
c     ---------------------------------------------------------------------------------------------------------------
c     STL files for interfaces
c     ---------------------------------------------------------------------------------------------------------------
      Ninterface=Nsolid+1
      do interface=1,Ninterface
c     produce the obj
         radius=R1              ! [m]
         do i=1,interface-1
            radius=radius+e_solid(i) ! [m]
         enddo                  ! i
         call duplicate_so(dim,Nv_unitysphere,Nf_unitysphere,v_unitysphere,f_unitysphere,Nv_so,Nf_so,v_so,f_so)
         call scale_obj(dim,Nv_so,v_so,radius)
         Nv=0
         Nf=0
         call add2obj(dim,Nv,Nf,v,f,.false.,Nv_so,Nf_so,v_so,f_so)
c     ---------------------------------------------------------------
c     OUT interface
c     ---------------------------------------------------------------
c     get interface name
         normal_out=.true.
         call sphere_name(interface,normal_out,interface_name)
         stl_file='./results/stardis_files/'//trim(interface_name)
c     record stl file
         invert_normal=.false.
         call obj2stl(dim,Nv,Nf,v,f,invert_normal,stl_file)
c     
         if (interface.eq.1) then
c     ---------------------------------------------------------------
c     IN interface
c     ---------------------------------------------------------------
c     get interface name
            normal_out=.false.
            call sphere_name(interface,normal_out,interface_name)
            stl_file='./results/stardis_files/'//trim(interface_name)
c     record stl file
            invert_normal=.true.
            call obj2stl(dim,Nv,Nf,v,f,invert_normal,stl_file)
         endif ! interface=1
      enddo                     ! interface
c     ---------------------------------------------------------------------------------------------------------------
c     STL files for solids
c     ---------------------------------------------------------------------------------------------------------------
      do isolid=1,Nsolid
c     produce the obj
         r_in=R1                ! [m]
         do i=1,isolid-1
            r_in=r_in+e_solid(i) ! [m]
         enddo                  ! i
         r_out=r_in+e_solid(isolid) ! [m]
         call duplicate_so(dim,Nv_unitysphere,Nf_unitysphere,v_unitysphere,f_unitysphere,Nv_so,Nf_so,v_so,f_so)
         call scale_obj(dim,Nv_so,v_so,r_in)
         Nv=0
         Nf=0
         call add2obj(dim,Nv,Nf,v,f,.true.,Nv_so,Nf_so,v_so,f_so)
         call duplicate_so(dim,Nv_unitysphere,Nf_unitysphere,v_unitysphere,f_unitysphere,Nv_so,Nf_so,v_so,f_so)
         call scale_obj(dim,Nv_so,v_so,r_out)
         call add2obj(dim,Nv,Nf,v,f,.false.,Nv_so,Nf_so,v_so,f_so)
c     get shell name
         call shell_name(isolid,solid_name)
         stl_file='./results/stardis_files/'//trim(solid_name)
c     record stl file
         invert_normal=.false.
         call obj2stl(dim,Nv,Nf,v,f,invert_normal,stl_file)
      enddo                     ! isolid
c     ---------------------------------------------------------------------------------------------------------------
c     Stardis model file
c     ---------------------------------------------------------------------------------------------------------------
      model_file='./results/stardis_files/model.txt'
      open(11,file=trim(model_file))
      write(11,10) '# Ambient radiative temperature / Reference radiative temperature'
      write(11,30) 'TRAD ',Trad,Trad_ref
      write(11,*)
      write(11,10) '# media'
      call sphere_name(1,.true.,interface_name)
      write(11,31) 'FLUID ','internal_fluid ',rho_fluid(1),C_fluid(1),Tf0,' UNKNOWN',' FRONT ',trim(interface_name)
      do isolid=1,Nsolid
         call num2str(isolid,solid_str)
         mat_str='mat'//trim(solid_str)
         call shell_name(isolid,solid_name)
         write(11,32) 'SOLID ',trim(mat_str),lambda_solid(isolid),rho_solid(isolid),C_solid(isolid),e_solid(isolid)/ddf,Tinit_solid(isolid),' UNKNOWN',P_solid(isolid),' FRONT ',trim(solid_name)
      enddo                     ! isolid
      write(11,*)
      write(11,10) '# Connections'
      call sphere_name(1,.false.,interface_name)
      interface_name=' '//trim(interface_name)
      write(11,33) 'SOLID_FLUID_CONNECTION ','solid_fluid ',Trad_ref,1.0,0.0,hc1,trim(interface_name)
      do itr=1,Ntr
         call num2str(itr,tr_str)
         tr_name='junction'//trim(tr_str)
         if (R(itr).gt.0.0D+0) then
            call sphere_name(itr+1,.true.,interface_name)
            interface_name=' '//trim(interface_name)
            write(11,34) 'SOLID_SOLID_CONNECTION ',trim(tr_name),R(itr),trim(interface_name)
         endif
      enddo                     ! itr
      write(11,*)
      write(11,10) '# Boundary conditions'
      call sphere_name(Ninterface,.true.,interface_name)
      interface_name=' '//trim(interface_name)
      write(11,35) 'H_BOUNDARY_FOR_SOLID ',' external_boundary ',Trad_ref,1.0,0.0,hc2,Tf2,trim(interface_name)
      close(11)
      write(*,*) 'Stardis model file was recorded: ',trim(model_file)
c     ---------------------------------------------------------------------------------------------------------------
c     Launch script
c     ---------------------------------------------------------------------------------------------------------------
      script_file='./results/stardis_files/run.sh'
      open(12,file=trim(script_file))
      write(12,10) '#!/bin/sh -e'
      write(12,*)
      write(12,10) '# USER PARAMETERS SECTION'
      call num2str(Nsample,Nsample_str)
      write(12,10) 'NREAL='//trim(Nsample_str)
      line='TIME="'
      do i=1,Ntime
         call time(i,Ntime,alpha,total_time,t)
         call dble2str_noexp(t,nap,time_str,err)
         if (err) then
            call error(label)
            write(*,*) 'Could not convert to character string: t=',t
            stop
         endif
         if (i.eq.1) then
            line=trim(line)//trim(time_str)
         else
            line=trim(line)//' '//trim(time_str)
         endif
      enddo                     ! i
      line=trim(line)//'"'
      write(12,10) trim(line)
      write(12,10) '# END USER PARAMETERS SECTION'
      write(12,*)
      write(12,10) '# Check stardis installation'
      write(12,10) 'if ! command -v stardis > /dev/null ; then'
      write(12,10) '  echo ">>> stardis command not found !"'
      write(12,10) '  echo ">>> To register stardis in the current shell you must type :"'
      write(12,10) '  echo ">>> source ~/Stardis-XXX-GNU-Linux64/etc/stardis.profile"'
      write(12,10) '  echo ">>> where ~/Stardis-XXX-GNU-Linux64 is the stardis directory installation"'
      write(12,10) '  exit 1'
      write(12,10) 'fi'
      write(12,*)
      write(12,10) '# Launch Stardis for each defined position and time'
      do iprobe=1,Nprobe
         Nfile=1
         call num2str3(iprobe,iprobe_str)
         do j=1,dim
            x(j)=probe_position(iprobe,j)
         enddo                  ! j
         call vector_length(dim,x,d)
c     looking for solid index
         if (d.lt.R1) then
            solid_idx=0         ! code for internal fluid
         else if (d.eq.R1) then
            solid_idx=1
         else if (d.eq.R2) then
            solid_idx=Nsolid
         else            
            solid_found=.false.
            is_junction=.false.
            sum_e=0.0D+0
            do isolid=1,Nsolid
               if ((d.gt.R1+sum_e).and.(d.lt.R1+sum_e+e_solid(isolid))) then
                  solid_found=.true.
                  solid_idx=isolid
                  goto 111
               endif
               if (d.eq.R1+sum_e+e_solid(isolid)) then
                  solid_found=.true.
                  solid_idx=isolid
                  is_junction=.true.
                  goto 111
               endif
               sum_e=sum_e+e_solid(isolid)
            enddo               ! isolid
 111        continue
            if (.not.solid_found) then
               call error(label)
               write(*,*) 'Solid could not be identified for probe position:',(x(j),j=1,dim)
               write(*,*) 'Distance to center=',d,' m'
               stop
            endif
            if ((is_junction).and.(R(solid_idx).gt.0.0D+0)) then
               Nfile=2
            endif
         endif ! d
c
         if (Nfile.eq.1) then
            write(12,10) 'FILE="stardis_result_N${NREAL}_position'//trim(iprobe_str)//'.txt"'
            write(12,10) '# Erase FILE result if exists'
            write(12,10) 'rm -f "${FILE}"'
            write(12,10) 'echo "#time Temperature  errorbar  N_failures N_Realizations" >> "${FILE}"'
            write(12,10) 'for i in ${TIME}; do'
            write(12,10) '  printf ''%s  '' "${i}"  >> "${FILE}" '
         else if (Nfile.eq.2) then
c     sligthly move x
            x(1)=x(1)+epsilon
            x(3)=x(3)+epsilon
            write(12,10) 'FILE1="stardis_result_N${NREAL}_position'//trim(iprobe_str)//'_1.txt"'
            write(12,10) 'FILE2="stardis_result_N${NREAL}_position'//trim(iprobe_str)//'_2.txt"'
            write(12,10) '# Erase FILE result if exists'
            write(12,10) 'rm -f "${FILE1}" "${FILE2}"'
            write(12,10) 'echo "#time Temperature  errorbar  N_failures N_Realizations" >> "${FILE1}"'
            write(12,10) 'echo "#time Temperature  errorbar  N_failures N_Realizations" >> "${FILE2}"'
            write(12,10) 'for i in ${TIME}; do'
            write(12,10) '  printf ''%s  '' "${i}"  >> "${FILE1}" '
            write(12,10) '  printf ''%s  '' "${i}"  >> "${FILE2}" '
         else
            call error(label)
            write(*,*) 'Nfile=',Nfile
            stop
         endif
c
         call num2str(solid_idx,solid_str)
         mat_str='mat'//trim(solid_str)
         call dble2str_noexp(x(1),nap,xstr,err)
         if (err) then
            call error(label)
            write(*,*) 'Could not convert to string: x(1)=',x(1)
            stop
         endif
         line1=' '//trim(xstr)//','
         call dble2str_noexp(x(2),nap,xstr,err)
         if (err) then
            call error(label)
            write(*,*) 'Could not convert to string: x(2)=',x(2)
            stop
         endif
         line1=trim(line1)//trim(xstr)//','
         call dble2str_noexp(x(3),nap,xstr,err)
         if (err) then
            call error(label)
            write(*,*) 'Could not convert to string: x(3)=',x(3)
            stop
         endif
         line1=trim(line1)//trim(xstr)

         if (d.lt.R1) then
            line='  stardis -V 3 -M model.txt -p'//trim(line1)//',"${i}" -n "${NREAL}" >> "${FILE}"'
            write(12,10) trim(line)
         else if (d.eq.R1) then
            line='  stardis -V 3 -M model.txt -P'//trim(line1)//',"${i}:'//trim(mat_str)//'" -n "${NREAL}" >> "${FILE}"'
            write(12,10) trim(line)
         else if (d.eq.R2) then
            line='  stardis -V 3 -M model.txt -P'//trim(line1)//',"${i}" -n "${NREAL}" >> "${FILE}"'
            write(12,10) trim(line)
         else
            if (is_junction) then
               if (R(solid_idx).eq.0.0D+0) then
                  line='  stardis -V 3 -M model.txt -P'//trim(line1)//',"${i}" -n "${NREAL}" >> "${FILE}"'
                  write(12,10) trim(line)
               else
                  line='  stardis -V 3 -M model.txt -P'//trim(line1)//',"${i}:'//trim(mat_str)//'" -n "${NREAL}" >> "${FILE1}"'
                  write(12,10) trim(line)
                  call num2str(solid_idx+1,solid_str)
                  mat_str='mat'//trim(solid_str)
                  line='  stardis -V 3 -M model.txt -P'//trim(line1)//',"${i}:'//trim(mat_str)//'" -n "${NREAL}" >> "${FILE2}"'
                  write(12,10) trim(line)
               endif            ! R(solid_idx)
            else
               line='  stardis -V 3 -M model.txt -p'//trim(line1)//',"${i}" -n "${NREAL}" >> "${FILE}"'
               write(12,10) trim(line)
            endif               ! is_junction
         endif                  ! d
         write(12,10) 'done'
         write(12,*)
c         
      enddo                     ! iprobe
      write(12,*)
      write(12,10) 'echo " "'
      write(12,10) 'echo ">>> Stardis simulation done"'
      close(12)
      write(*,*) 'Stardis script file was recorded: ',trim(script_file)
      command='chmod u+x '//trim(script_file)
      call exec(command)

      
      return
      end

      

      subroutine sphere_name(interface_index,normal_out,filename)
      implicit none
      include 'max.inc'
c     
c     Purpose: to produce a file name for a sphere (interface)
c     
c     Input:
c       + interface_index: index of interface [1, Nsolid+1]
c       + normal_out: when T, normal points to the outside of the sphere; when F, normal points inside
c     
c     Output:
c       + filename: file name
c     
c     I/O
      integer interface_index
      logical normal_out
      character*(Nchar_mx) filename
c     temp
      character*10 int_str
c     label
      character*(Nchar_mx) label
      label='subroutine sphere_name'

      filename='sphere'
      call num2str(interface_index,int_str)
      filename=trim(filename)//'_r'//trim(int_str)
      if (normal_out) then
         filename=trim(filename)//'_out'
      else
         filename=trim(filename)//'_in'
      endif
      filename=trim(filename)//'.stl'

      return
      end

      

      subroutine shell_name(solid_index,filename)
      implicit none
      include 'max.inc'
c     
c     Purpose: to produce a file name for a shell (solid)
c     
c     Input:
c       + solid_index: index of solid [1, Nsolid]
c     
c     Output:
c       + filename: file name
c     
c     I/O
      integer solid_index
      character*(Nchar_mx) filename
c     temp
      character*10 solid_str
c     label
      character*(Nchar_mx) label
      label='subroutine shell_name'

      filename='shell'
      call num2str(solid_index,solid_str)
      filename=trim(filename)//trim(solid_str)//'.stl'

      return
      end
