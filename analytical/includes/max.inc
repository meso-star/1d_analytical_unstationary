          integer Ndim_mx
          integer Nfluid_mx
          integer Nsolid_mx
	  integer Ntr_mx
	  integer Nprobe_mx
	  integer Ntime_mx
	  integer Nv_mx
	  integer Nf_mx
	  integer Nv_so_mx
	  integer Nf_so_mx
	  integer Nchar_mx

          parameter(Ndim_mx=3)
	  parameter(Nfluid_mx=3)
	  parameter(Nsolid_mx=100)
	  parameter(Ntr_mx=Nsolid_mx-1)
	  parameter(Nprobe_mx=100)
	  parameter(Ntime_mx=100)
          parameter(Nv_mx=10000000)
          parameter(Nf_mx=20000000)
          parameter(Nv_so_mx=260000)
          parameter(Nf_so_mx=520000)
	  parameter(Nchar_mx=10000)
